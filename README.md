# Tribes

Tribes is a social hosting service

## Development

To start the Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Install Node.js dependencies with `npm install` inside the `assets` directory
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Provisioning the cluster

1. Install Ubuntu 20.04.
2. Install MicroK8s: `sudo snap install microk8s --classic --channel=1.19`
3. Enable addons: `microk8s enable dns metallb storage dashboard`
4. Install [Contour](https://projectcontour.io/): `microk8s kubectl apply -f https://projectcontour.io/quickstart/contour.yaml`
5. Install [cert-manager](https://cert-manager.io/): `microk8s kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.1.0/cert-manager.yaml`
6. Install the Tribes ClusterIssuer: `microk8s kubectl apply -f https://gitlab.com/tribes-host/tribes/-/raw/develop/installation/k8s/cluster-issuer.yml`
7. Install [Longhorn](https://longhorn.io/): `microk8s kubectl apply -f https://raw.githubusercontent.com/longhorn/longhorn/v1.1.0/deploy/longhorn.yaml`
8. Install Tribes: `microk8s kubectl apply -f https://gitlab.com/tribes-host/tribes/-/raw/develop/installation/k8s/tribeshost.yaml`
9. Expose K8S config to Tribes: `microk8s kubectl create -n tribeshost secret generic tribeshost-k8s-config --from-file=config=/var/snap/microk8s/current/credentials/client.config`
10. Create a `runtime.exs` and expose it: `kubectl create -n tribeshost secret generic tribeshost-config --from-file=./config/runtime.exs` (TODO: better instructions)

## Recovery

In the event of disaster, here are some recovery options.

### When the cluster stops responding

After [changing the LAN IP address of one or more nodes, the whole cluster may stop responding](https://github.com/ubuntu/microk8s/issues/1955).
So far, no solution has been found.

It is recommended to take preventative measures by [backing up the cluster](https://github.com/vmware-tanzu/velero) so it can be restored.

It may be easier to perform recovery on Kubernetes distributions other than MicroK8s.
MicroK8s uses [Dqlite](https://dqlite.io/) as its datastore, which so far we have been unable to figure out how to query.

### Database corruption

If the database gets corrupted, eg [`PANIC:  could not locate a valid checkpoint record`](https://stackoverflow.com/a/8812934):

```
$ kubectl logs -n tribeshost pod/tribeshost-deployment-579cf6bcb7-bwfcj postgres

PostgreSQL Database directory appears to contain a database; Skipping initialization

2021-03-27 21:59:07.544 UTC [1] LOG:  starting PostgreSQL 13.1 on x86_64-pc-linux-musl, compiled by gcc (Alpine 10.2.1_pre1) 10.2.1 20201203, 64-bit
2021-03-27 21:59:07.545 UTC [1] LOG:  listening on IPv4 address "0.0.0.0", port 5432
2021-03-27 21:59:07.545 UTC [1] LOG:  listening on IPv6 address "::", port 5432
2021-03-27 21:59:07.570 UTC [1] LOG:  listening on Unix socket "/var/run/postgresql/.s.PGSQL.5432"
2021-03-27 21:59:07.604 UTC [21] LOG:  database system was shut down at 2021-03-27 19:39:32 UTC
2021-03-27 21:59:07.605 UTC [21] LOG:  invalid resource manager ID in primary checkpoint record
2021-03-27 21:59:07.605 UTC [21] PANIC:  could not locate a valid checkpoint record
2021-03-27 21:59:08.101 UTC [22] FATAL:  the database system is starting up
2021-03-27 21:59:08.102 UTC [23] FATAL:  the database system is starting up
2021-03-27 21:59:08.569 UTC [1] LOG:  startup process (PID 21) was terminated by signal 6: Aborted
2021-03-27 21:59:08.569 UTC [1] LOG:  aborting startup due to startup process failure
2021-03-27 21:59:08.574 UTC [1] LOG:  database system is shut down
```

...the Postgres container will not run at all, so you will be unable to fix the issue.

In this case, you should destroy the deployment and run a temporary recovery deployment, `recovery.yaml`, found in this repository:

```
$ kubectl delete deployment -n tribeshost tribeshost-deployment
deployment.apps "tribeshost-deployment" deleted
$ kubectl apply -f installation/k8s/recovery.yaml  
deployment.apps/tribeshost-recovery created
```

Find the pod:

```
$ kubectl get pods -n tribeshost                                                             
NAME                                  READY   STATUS    RESTARTS   AGE
tribeshost-recovery-c6489487d-zv9fl   2/2     Running   0          13m
```

You can then shell into the pod and run commands:

```
$ kubectl exec -it -n tribeshost tribeshost-recovery-c6489487d-zv9fl -c postgres -- /bin/sh
/ # su postgres
/ $ pg_resetwal $DATADIR
```
