# Modified from Pleroma, test/support/helpers.ex
# Copyright © 2017-2020 Pleroma Authors <https://pleroma.social/>
# SPDX-License-Identifier: AGPL-3.0-only

defmodule Tribes.Tests.Helpers do
  @moduledoc """
  Helpers for use in tests.
  """
  alias Tribes.Config

  defmacro clear_config(config_path) do
    quote do
      clear_config(unquote(config_path)) do
      end
    end
  end

  defmacro clear_config(config_path, do: yield) do
    quote do
      initial_setting = Config.fetch(unquote(config_path))
      unquote(yield)

      on_exit(fn ->
        case initial_setting do
          :error ->
            Config.delete(unquote(config_path))

          {:ok, value} ->
            Config.put(unquote(config_path), value)
        end
      end)

      :ok
    end
  end

  defmacro clear_config(config_path, temp_setting) do
    quote do
      clear_config(unquote(config_path)) do
        Config.put(unquote(config_path), unquote(temp_setting))
      end
    end
  end

  defmacro __using__(_opts) do
    quote do
      import Tribes.Tests.Helpers,
        only: [
          clear_config: 1,
          clear_config: 2
        ]
    end
  end
end
