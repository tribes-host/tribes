defmodule Tribes.K8sMock do
  @base_url "https://k8s.tribes.test"
  @ns "tribe-cffssicjqdgqjmsaclkremq"
  @ns_base_url "#{@base_url}/api/v1/namespaces"
  @ns_url "#{@ns_base_url}/#{@ns}"

  def request(:get, _, _, _, _), do: resource404()
  def request(:post, _, body, _, _), do: {:ok, %HTTPoison.Response{status_code: 200, body: body}}
  def request(:delete, @ns_url, _, _, _), do: file200("ns-delete-200.json")

  defp file200(filename) do
    body = File.read!("test/fixtures/k8s/#{filename}")
    {:ok, %HTTPoison.Response{status_code: 200, body: body}}
  end

  defp resource404() do
    body = File.read!("test/fixtures/k8s/resource-404.json")
    {:ok, %HTTPoison.Response{status_code: 404, body: body}}
  end
end
