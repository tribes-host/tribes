defmodule Tribes.HTTPoisonMock do
  def get("https://gleasonator.com/api/v1/instance") do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/gleasonator-instance.json"),
       status_code: 200
     }}
  end

  def get("https://gleasonator.com/api/v1/accounts/dave") do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/gleasonator-dave.json"),
       status_code: 200
     }}
  end

  def get("https://gleasonator.com/api/v1/accounts/alex") do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/gleasonator-alex.json"),
       status_code: 200
     }}
  end

  def get("https://gleasonator.com/api/v1/timelines/public?local=true") do
    {:ok,
     %HTTPoison.Response{
       body: File.read!("test/fixtures/gleasonator-timeline.json"),
       status_code: 200
     }}
  end
end
