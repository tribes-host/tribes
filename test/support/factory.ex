defmodule Tribes.Factory do
  use ExMachina.Ecto, repo: Tribes.Repo
  alias Tribes.Instance
  alias Tribes.User
  alias TribesPay.Payment
  alias TribesPay.PaymentRequest
  alias TribesPay.Subscription
  alias TribesWeb.Session

  def instance_factory do
    user = insert(:user)

    %Instance{
      domain: sequence(:domain, &"my-テスト#{&1}.tld"),
      user_id: user.id
    }
  end

  def user_factory do
    %User{
      email: sequence(:email, &"user#{&1}@my-テスト#{&1}.tld"),
      password_hash: Pbkdf2.hash_pwd_salt("test")
    }
  end

  def session_factory do
    user = insert(:user)

    %Session{
      token: Session.generate_token(),
      user_id: user.id,
      user: user
    }
  end

  def payment_factory(attrs) do
    pr = attrs[:payment_request] || insert(:payment_request)

    p = %Payment{
      user_id: pr.user_id,
      payment_request_id: pr.id,
      amount: Money.new(2695, :BTC),
      amount_converted: pr.amount,
      gateway: :btcpay,
      gateway_data: %{}
    }

    attrs = Map.delete(attrs, :payment_request)
    merge_attributes(p, attrs)
  end

  def payment_request_factory(attrs) do
    user = attrs[:user] || insert(:user)

    %PaymentRequest{
      status: attrs[:status] || :unpaid,
      user_id: attrs[:user_id] || user.id,
      subscription_id: attrs[:subscription_id] || insert(:subscription, user: user).id,
      amount: attrs[:amount] || Money.new(1000, :USD),
      billing_period: attrs[:billing_period] || "2021-1"
    }
  end

  def subscription_factory(attrs) do
    user = attrs[:user] || insert(:user)

    %Subscription{
      status: attrs[:status] || :active,
      tier: attrs[:tier] || :small,
      billing_day: attrs[:billing_day] || 1,
      user_id: user.id
    }
  end
end
