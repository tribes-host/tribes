defmodule TribesDeployTest do
  alias Tribes.Instance
  use Tribes.DataCase

  setup do
    K8s.Client.DynamicHTTPProvider.register(self(), Tribes.K8sMock)
  end

  test "deploy_instance/1" do
    instance = %Instance{id: "A1tpmBi786uXPxp0uu", domain: "tribes.test"}
    assert {:ok, [{:ok, _} | _]} = TribesDeploy.deploy_instance(instance)
  end

  test "destroy_instance/1" do
    instance = %Instance{id: "A1tpmBi786uXPxp0uu", domain: "tribes.test"}
    assert {:ok, _} = TribesDeploy.destroy_instance(instance)
  end
end
