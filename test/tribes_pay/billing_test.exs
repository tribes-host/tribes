defmodule TribesPay.BillingTest do
  alias Tribes.Repo
  alias TribesPay.Billing
  alias TribesPay.PaymentRequest
  alias TribesPay.Subscription
  use Tribes.DataCase
  import Tribes.Factory

  @tag timeout: :infinity
  test "unbilled_subscriptions_query/1" do
    today = Date.new!(2020, 12, 15)

    insert_list(5, :subscription, status: :cancelled)

    unbilled_new_subscriptions =
      Enum.map(1..20, fn _ ->
        insert(:subscription, billing_day: Enum.random(1..15), status: :active)
        |> Repo.preload(:user)
      end)

    unbilled_prior_subscriptions =
      Enum.map(1..5, fn _ ->
        subscription = insert(:subscription, billing_day: Enum.random(1..15), status: :active)

        insert(:payment_request,
          status: :paid,
          billing_period: "2020-11",
          subscription_id: subscription.id
        )

        Repo.preload(subscription, :user)
      end)

    billed_subscriptions =
      Enum.map(1..10, fn _ ->
        subscription = insert(:subscription, billing_day: Enum.random(1..15), status: :active)

        insert(:payment_request,
          status: :unpaid,
          billing_period: "2020-12",
          subscription_id: subscription.id
        )

        Repo.preload(subscription, :user)
      end)

    unbilled_undue_subscriptions =
      Enum.map(1..12, fn _ ->
        insert(:subscription, billing_day: Enum.random(16..31), status: :active)
        |> Repo.preload(:user)
      end)

    # Sanity check
    assert Repo.all(Subscription) |> Enum.count() == 52
    assert Repo.all(PaymentRequest) |> Enum.count() == 15

    result =
      today
      |> Billing.unbilled_subscriptions_query()
      |> Repo.all()

    assert Enum.all?(unbilled_new_subscriptions, &(&1 in result))
    assert Enum.all?(unbilled_prior_subscriptions, &(&1 in result))
    refute Enum.any?(billed_subscriptions, &(&1 in result))
    refute Enum.any?(unbilled_undue_subscriptions, &(&1 in result))
    assert Enum.count(result) == 25
  end
end
