defmodule TribesPay.SubscriptionTest do
  alias TribesPay.Subscription
  use Tribes.DataCase
  import Tribes.Factory

  test "to_price/1 returns the correct price by tier" do
    assert %Money{amount: 1000, currency: :USD} ==
             insert(:subscription, tier: :small) |> Subscription.to_price()
  end
end
