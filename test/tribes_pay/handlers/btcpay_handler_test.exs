defmodule TribesPay.BTCPayHandlerTest do
  use Tribes.DataCase
  alias TribesPay.BTCPayHandler
  import Tribes.Factory

  defmodule Client do
    @store "8d4Uhm8Tb898LavnfKyUAApKJ2vWe1AW7VqnaQoz73ed"

    def get("/api/v1/stores/#{@store}/invoices/EFJMXaxnMrPfY3wFBm6q6q"),
      do: file200("invoice.json")

    defp file200(filename) do
      body = File.read!("test/fixtures/btcpay/#{filename}")
      {:ok, %HTTPoison.Response{status_code: 200, body: body}}
    end
  end

  describe "handle_event/1 with InvoiceReceivedPayment" do
    setup do
      Application.put_env(:btcpay, :http_client, __MODULE__.Client)

      s = insert(:subscription)

      {:ok, pr} =
        build(:payment_request, subscription_id: s.id)
        |> Map.put(:id, "A24tZ8KMyj5y6PmXVg")
        |> Tribes.Repo.insert()

      insert(:instance, subscription_id: s.id)

      event = %{
        "type" => "InvoiceReceivedPayment",
        "storeId" => "8d4Uhm8Tb898LavnfKyUAApKJ2vWe1AW7VqnaQoz73ed",
        "invoiceId" => "EFJMXaxnMrPfY3wFBm6q6q",
        "overPaid" => false,
        "deliveryId" => "9XWKhRnr73m2JLEkNrVELF",
        "webhookId" => "GveBmW1eWs3hmcg1emumpw",
        "orignalDeliveryId" => "9XWKhRnr73m2JLEkNrVELF",
        "isRedelivery" => false,
        "timestamp" => 1_607_451_803
      }

      {:ok, %{event: event, payment_request: pr}}
    end

    test "triggers a deploy", %{event: event} do
      assert :ok == BTCPayHandler.handle_event(event)
      assert_enqueued(worker: TribesDeploy.DeployWorker)
    end
  end
end
