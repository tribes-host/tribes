defmodule TribesPay.PaymentTest do
  use Tribes.DataCase
  alias TribesPay.Payment
  alias TribesPay.PaymentRequest
  import Tribes.Factory

  test "create/1 marks the PaymentRequest paid" do
    s = insert(:subscription)
    pr = insert(:payment_request, amount: Money.new(1000, :USD), subscription_id: s.id)
    p = build(:payment, amount_converted: Money.new(1000, :USD), payment_request: pr)
    insert(:instance, subscription_id: s.id)

    assert {:ok, %Payment{}, %PaymentRequest{status: :paid}} = Payment.create(p)
    assert %PaymentRequest{status: :paid} = PaymentRequest.get_by_id(pr.id)
  end

  test "create/1 leaves PaymentRequest unpaid if amount isn't high enough" do
    pr = insert(:payment_request, amount: Money.new(1000, :USD))
    p = build(:payment, amount_converted: Money.new(900, :USD), payment_request: pr)

    assert {:ok, %Payment{}, _} = Payment.create(p)
    assert %PaymentRequest{status: :unpaid} = PaymentRequest.get_by_id(pr.id)
  end

  test "create/1 considers multiple payments" do
    s = insert(:subscription)
    pr = insert(:payment_request, amount: Money.new(1000, :USD), subscription_id: s.id)
    p = build(:payment, amount_converted: Money.new(900, :USD), payment_request: pr)
    p2 = build(:payment, amount_converted: Money.new(100, :USD), payment_request: pr)
    insert(:instance, subscription_id: s.id)

    assert {:ok, %Payment{}, _} = Payment.create(p)
    assert {:ok, %Payment{}, _} = Payment.create(p2)
    assert %PaymentRequest{status: :paid} = PaymentRequest.get_by_id(pr.id)
  end

  test "sum_converted/1 sums the amounts from list of Payments" do
    pr = insert(:payment_request, amount: Money.new(1000, :USD))

    payments =
      insert_list(5, :payment, amount_converted: Money.new(100, :USD), payment_request: pr)

    assert Payment.sum_converted(payments, :USD) == %Money{amount: 500, currency: :USD}
  end

  test "initial payment causes an instance to be deployed" do
    s = insert(:subscription)
    pr = insert(:payment_request, amount: Money.new(1000, :USD), subscription_id: s.id)
    instance = insert(:instance, subscription_id: s.id, status: :draft)

    %Payment{
      user_id: instance.user_id,
      payment_request_id: pr.id,
      amount: Money.new(1000, :USD),
      amount_converted: Money.new(1000, :USD),
      gateway: :btcpay,
      gateway_data: %{}
    }
    |> Payment.create()

    assert_enqueued(
      worker: TribesDeploy.DeployWorker,
      args: %{instance_id: instance.id, user_id: instance.user_id, domain: instance.domain}
    )
  end
end
