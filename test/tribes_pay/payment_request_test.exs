defmodule TribesPay.PaymentRequestTest do
  use Tribes.DataCase
  alias TribesPay.PaymentRequest
  import Tribes.Factory

  test "get_payments/1 returns all Payments for the PaymentRequest" do
    pr = insert(:payment_request, amount: Money.new(1000, :USD))

    payments =
      insert_list(5, :payment, amount_converted: Money.new(1000, :USD), payment_request: pr)

    assert PaymentRequest.get_payments(pr) == payments
  end

  test "refresh_status/1 marks the PaymentRequest paid" do
    pr = insert(:payment_request, amount: Money.new(1000, :USD))
    insert(:payment, amount_converted: Money.new(1000, :USD), payment_request: pr)

    assert {:ok, %PaymentRequest{status: :paid}} = PaymentRequest.refresh_status(pr)
  end

  test "refresh_status/1 leaves PaymentRequest unpaid if amount isn't high enough" do
    pr = insert(:payment_request, amount: Money.new(1000, :USD))
    insert(:payment, amount_converted: Money.new(900, :USD), payment_request: pr)

    assert {:ok, %PaymentRequest{status: :unpaid}} = PaymentRequest.refresh_status(pr)
  end

  test "refresh_status/1 considers multiple payments" do
    pr = insert(:payment_request, amount: Money.new(1000, :USD))
    insert(:payment, amount_converted: Money.new(900, :USD), payment_request: pr)
    insert(:payment, amount_converted: Money.new(100, :USD), payment_request: pr)

    assert {:ok, %PaymentRequest{status: :paid}} = PaymentRequest.refresh_status(pr)
  end

  test "refresh_status/1 reverts a paid status if payments don't add up" do
    pr = insert(:payment_request, amount: Money.new(1000, :USD), status: :paid)

    assert {:ok, %PaymentRequest{status: :unpaid}} = PaymentRequest.refresh_status(pr)
  end
end
