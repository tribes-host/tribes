defmodule TribesDeploy.DeployWorkerTest do
  use Tribes.DataCase
  alias Tribes.Instance
  alias Tribes.Repo
  alias Tribes.Tests.ObanHelpers
  alias TribesDeploy.DeployWorker
  alias TribesDeploy.PingWorker
  import Tribes.Factory

  setup do
    K8s.Client.DynamicHTTPProvider.register(self(), Tribes.K8sMock)
  end

  test "perform/1" do
    user = insert(:user)

    {:ok, instance} =
      %Instance{id: "A1tpmBi786uXPxp0uu", domain: "tribes.test", user_id: user.id}
      |> Repo.insert()

    {:ok, %Oban.Job{} = job} = DeployWorker.enqueue(instance)

    assert_enqueued(
      worker: DeployWorker,
      args: %{
        instance_id: "A1tpmBi786uXPxp0uu",
        domain: "tribes.test",
        user_id: user.id
      }
    )

    assert {:ok, [{:ok, _} | _]} = ObanHelpers.perform(job)
    assert_enqueued(worker: PingWorker)

    instance = Instance.get_by_id("A1tpmBi786uXPxp0uu")
    assert instance.status == :deploying
  end
end
