defmodule TribesDeploy.DestroyWorkerTest do
  use Tribes.DataCase
  alias Tribes.Instance
  alias Tribes.Repo
  alias Tribes.Tests.ObanHelpers
  alias TribesDeploy.DestroyWorker
  import Tribes.Factory

  setup do
    K8s.Client.DynamicHTTPProvider.register(self(), Tribes.K8sMock)
  end

  test "perform/1" do
    user = insert(:user)

    {:ok, instance} =
      %Instance{id: "A1tpmBi786uXPxp0uu", domain: "tribes.test", user_id: user.id}
      |> Repo.insert()

    {:ok, %Oban.Job{} = job} = DestroyWorker.enqueue(instance)

    assert_enqueued(
      worker: DestroyWorker,
      args: %{
        instance_id: "A1tpmBi786uXPxp0uu",
        domain: "tribes.test",
        user_id: user.id
      }
    )

    assert :ok == ObanHelpers.perform(job)

    assert Instance.get_by_id("A1tpmBi786uXPxp0uu") == nil
  end
end
