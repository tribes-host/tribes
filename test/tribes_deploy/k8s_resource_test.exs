defmodule Tribes.K8sResourceTest do
  use Tribes.DataCase
  alias TribesDeploy.K8sResource
  import Tribes.Factory

  test "volume returns the correct name" do
    instance = insert(:instance)

    assert %{"metadata" => %{"name" => "tribe-volume-db"}} =
             K8sResource.generate_resource({:volume, :db}, instance)

    assert %{"metadata" => %{"name" => "tribe-volume-media"}} =
             K8sResource.generate_resource({:volume, :media}, instance)
  end
end
