ExUnit.start()
Ecto.Adapters.SQL.Sandbox.mode(Tribes.Repo, :manual)
K8s.Client.DynamicHTTPProvider.start_link(nil)
{:ok, _} = Application.ensure_all_started(:ex_machina)
