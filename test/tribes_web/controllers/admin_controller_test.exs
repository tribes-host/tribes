defmodule TribesWeb.AdminControllerTest do
  use TribesWeb.ConnCase
  import Tribes.Factory
  alias TribesWeb.Session

  setup do: clear_config([:registrations_open], true)

  test "admin can view index", %{conn: conn} do
    user = insert(:user, admin: true)
    {:ok, session} = Session.create(user)

    conn =
      conn
      |> put_session(:session_token, session.token)
      |> get("/admin")

    assert response(conn, 200)
  end

  test "non-admin can't view index", %{conn: conn} do
    user = insert(:user, admin: false)
    {:ok, session} = Session.create(user)

    conn =
      conn
      |> put_session(:session_token, session.token)
      |> get("/admin")

    assert redirected_to(conn) == "/login"
  end
end
