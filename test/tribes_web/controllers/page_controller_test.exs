defmodule TribesWeb.PageControllerTest do
  use TribesWeb.ConnCase

  test "GET /", %{conn: conn} do
    conn = get(conn, "/")
    assert html_response(conn, 200) =~ "Social media owned by you"
  end
end
