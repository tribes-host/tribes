defmodule TribesWeb.AuthControllerTest do
  use TribesWeb.ConnCase
  import Tribes.Factory
  alias Tribes.User
  alias TribesWeb.Session

  setup do: clear_config([:registrations_open], true)

  test "GET /register while logged out", %{conn: conn} do
    conn = get(conn, "/register")
    assert response(conn, 200)
  end

  test "GET /register while logged in", %{conn: conn} do
    session = insert(:session)

    conn =
      conn
      |> put_session(:session_token, session.token)
      |> get("/register")

    assert redirected_to(conn) == "/"
  end

  test "GET /register when registrations are closed", %{conn: conn} do
    clear_config([:registrations_open], false)
    conn = get(conn, "/register")
    assert redirected_to(conn) == "/login"
  end

  test "POST /register", %{conn: conn} do
    conn =
      post(conn, "/register", %{
        email: "test@testing123.com",
        password: "test",
        password_confirmation: "test",
        accepts_tos: "true",
        _csrf_token: Phoenix.Controller.get_csrf_token()
      })

    session = get_session(conn, :session_token) |> Session.get_by_token()

    %User{} = user = User.get_by_email("test@testing123.com")
    assert redirected_to(conn) == "/"
    assert session.user_id == user.id
  end

  test "POST /register with email already in use", %{conn: conn} do
    insert(:user, email: "test@testing123.com")

    conn =
      post(conn, "/register", %{
        email: "test@testing123.com",
        password: "test",
        password_confirmation: "test",
        _csrf_token: Phoenix.Controller.get_csrf_token()
      })

    assert redirected_to(conn) == "/register"
    refute get_session(conn, :session_token)
    assert get_flash(conn, :error)
  end

  test "POST /register with wrong password confirmation", %{conn: conn} do
    conn =
      post(conn, "/register", %{
        email: "test@testing123.com",
        password: "test",
        password_confirmation: "testt",
        _csrf_token: Phoenix.Controller.get_csrf_token()
      })

    refute User.get_by_email("test@testing123.com")
    assert redirected_to(conn) == "/register"
    refute get_session(conn, :session_token)
    assert get_flash(conn, :error)
  end

  test "POST /register when registrations are closed", %{conn: conn} do
    clear_config([:registrations_open], false)

    conn =
      post(conn, "/register", %{
        email: "test@testing123.com",
        password: "test",
        password_confirmation: "test",
        _csrf_token: Phoenix.Controller.get_csrf_token()
      })

    refute User.get_by_email("test@testing123.com")
    assert redirected_to(conn) == "/login"
  end

  test "GET /login while logged out", %{conn: conn} do
    conn = get(conn, "/login")
    assert response(conn, 200)
  end

  test "GET /login while logged in", %{conn: conn} do
    session = insert(:session)

    conn =
      conn
      |> put_session(:session_token, session.token)
      |> get("/login")

    assert redirected_to(conn) == "/"
  end

  test "POST /login", %{conn: conn} do
    user = insert(:user)

    conn =
      post(conn, "/login", %{
        email: user.email,
        password: "test",
        _csrf_token: Phoenix.Controller.get_csrf_token()
      })

    session = get_session(conn, :session_token) |> Session.get_by_token()

    assert redirected_to(conn) == "/"
    assert session.user_id == user.id
  end

  test "POST /login with wrong password", %{conn: conn} do
    user = insert(:user)

    conn =
      post(conn, "/login", %{
        email: user.email,
        password: "wrong password",
        _csrf_token: Phoenix.Controller.get_csrf_token()
      })

    assert redirected_to(conn) == "/login"
    refute get_session(conn, :session_token)
  end

  test "GET /logout while logged in", %{conn: conn} do
    session = insert(:session)

    conn =
      conn
      |> put_session(:session_token, session.token)
      |> get("/logout")

    assert redirected_to(conn) == "/"
    refute get_session(conn, :session_token)
  end

  test "GET /logout while logged out", %{conn: conn} do
    conn = get(conn, "/logout")
    assert redirected_to(conn) == "/"
  end

  test "GET /verify_email/:user_id/:token", %{conn: conn} do
    {:ok, %{user: user}} = User.register(%{email: "test@testing123.com", password: "testing"})
    conn = get(conn, "/verify_email/#{user.id}/#{user.email_verification_token}")
    session = get_session(conn, :session_token) |> Session.get_by_token()
    user = User.get_by_id(user.id)

    assert user.email_verified
    refute user.email_verification_token
    assert get_flash(conn, :success)
    assert redirected_to(conn) == "/"
    assert session.user_id == user.id
  end

  test "GET /verify_email/:user_id/:token with invalid params", %{conn: conn} do
    conn = get(conn, "/verify_email/123/456")
    assert get_flash(conn, :error)
    assert redirected_to(conn) == "/"
    refute get_session(conn, :session_token)
  end

  test "GET /verify_email/:user_id/:token with an old token", %{conn: conn} do
    {:ok, %{user: user}} = User.register(%{email: "test@testing123.com", password: "testing"})

    {:ok, user} =
      user
      |> Ecto.Changeset.change(email_verification_token_emailed_at: ~N[1993-07-03 00:00:00])
      |> Tribes.Repo.update()

    conn = get(conn, "/verify_email/#{user.id}/#{user.email_verification_token}")

    assert redirected_to(conn) == "/"
    refute get_session(conn, :session_token)
    assert get_flash(conn, :error)
    refute user.email_verified
    assert user.email_verification_token
  end

  test "GET /password_reset", %{conn: conn} do
    conn = get(conn, "/password_reset")
    assert response(conn, 200)
  end

  test "POST /password_reset", %{conn: conn} do
    user = insert(:user)
    conn = post(conn, "/password_reset", %{email: user.email})

    user = User.get_by_id(user.id)
    assert user.password_reset_token
    assert user.password_reset_token_emailed_at
    assert get_flash(conn, :info)
    assert redirected_to(conn) == "/"
  end

  test "POST /password_reset with nonexistent email", %{conn: conn} do
    conn = post(conn, "/password_reset", %{email: "yolo@yolo.me"})
    assert get_flash(conn, :error)
    assert redirected_to(conn) == "/password_reset"
  end

  test "GET /password_reset/:user_id/:token", %{conn: conn} do
    user = insert(:user)
    {:ok, user} = User.send_password_reset(user)

    conn = get(conn, "/password_reset/#{user.id}/#{user.password_reset_token}")
    assert response(conn, 200)
  end

  test "POST /password_reset/:user_id/:token", %{conn: conn} do
    user = insert(:user)
    {:ok, user} = User.send_password_reset(user)

    conn =
      post(conn, "/password_reset/#{user.id}/#{user.password_reset_token}", %{
        password: "yoooo",
        password_confirmation: "yoooo",
        _csrf_token: Phoenix.Controller.get_csrf_token()
      })

    user = User.get_by_id(user.id)
    session = get_session(conn, :session_token) |> Session.get_by_token()

    assert redirected_to(conn) == "/"
    assert session.user_id == user.id
    assert get_flash(conn, :success)
    assert Pbkdf2.verify_pass("yoooo", user.password_hash)
  end

  test "POST /password_reset/:user_id/:token with an old token", %{conn: conn} do
    user = insert(:user)
    {:ok, user} = User.send_password_reset(user)

    {:ok, user} =
      user
      |> Ecto.Changeset.change(password_reset_token_emailed_at: ~N[1993-07-03 00:00:00])
      |> Tribes.Repo.update()

    conn =
      post(conn, "/password_reset/#{user.id}/#{user.password_reset_token}", %{
        password: "yoooo",
        password_confirmation: "yoooo",
        _csrf_token: Phoenix.Controller.get_csrf_token()
      })

    assert redirected_to(conn) == "/"
    refute get_session(conn, :session_token1)
    assert get_flash(conn, :error)
    refute Pbkdf2.verify_pass("yoooo", user.password_hash)
  end
end
