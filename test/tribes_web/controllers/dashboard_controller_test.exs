defmodule TribesWeb.DashboardControllerTest do
  use TribesWeb.ConnCase
  import Tribes.Factory

  defmodule BTCPayClient do
    def post(_url, body), do: {:ok, %HTTPoison.Response{status_code: 200, body: body}}
  end

  test "GET /dashboard", %{conn: conn} do
    conn = get(conn, "/dashboard")
    assert redirected_to(conn) == "/login"
  end

  test "GET /dashboard when logged in", %{conn: conn} do
    user = insert(:user)

    conn =
      conn
      |> assign(:user, user)
      |> get("/dashboard")

    assert response(conn, 200)
  end

  test "GET /dashboard when logged in with tribe", %{conn: conn} do
    user = insert(:user)
    instance = insert(:instance, user_id: user.id, status: :online)

    conn =
      conn
      |> assign(:user, user)
      |> get("/dashboard")

    assert redirected_to(conn) == "/dashboard/#{instance.domain}"
  end

  test "GET /dashboard/:domain with wrong owner", %{conn: conn} do
    user = insert(:user)
    instance = insert(:instance)

    conn =
      conn
      |> assign(:user, user)
      |> get("/dashboard/#{instance.domain}")

    assert redirected_to(conn) == "/dashboard"
  end

  test "GET /dashboard/:domain/manage with wrong owner", %{conn: conn} do
    user = insert(:user)
    instance = insert(:instance)

    conn =
      conn
      |> assign(:user, user)
      |> get("/dashboard/#{instance.domain}/manage")

    assert redirected_to(conn) == "/dashboard"
  end

  test "GET /dashboard/:domain/billing with wrong owner", %{conn: conn} do
    user = insert(:user)
    instance = insert(:instance)

    conn =
      conn
      |> assign(:user, user)
      |> get("/dashboard/#{instance.domain}/billing")

    assert redirected_to(conn) == "/dashboard"
  end

  test "GET /dashboard/:domain/debugging with wrong owner", %{conn: conn} do
    user = insert(:user)
    instance = insert(:instance)

    conn =
      conn
      |> assign(:user, user)
      |> get("/dashboard/#{instance.domain}/debugging")

    assert redirected_to(conn) == "/dashboard"
  end

  describe "creating an instance" do
    test "creates a PaymentRequest", %{conn: conn} do
      Application.put_env(:btcpay, :http_client, __MODULE__.BTCPayClient)
      user = insert(:user)

      conn
      |> assign(:user, user)
      |> post("/dashboard/create", %{domain: "yolo.com", tier: "small"})

      assert Tribes.Repo.all(TribesPay.PaymentRequest) |> Enum.count() == 1
    end
  end
end
