defmodule TribesWeb.Plugs.RequireAdminPlugTest do
  use TribesWeb.ConnCase
  use ExUnit.Case, async: true
  use Plug.Test
  alias TribesWeb.Plugs.RequireAdminPlug
  import Tribes.Factory

  test "redirects to login when no user is assigned" do
    conn = build_conn()
    result = RequireAdminPlug.call(conn, nil)
    assert redirected_to(result) == "/login"
  end

  test "redirects to login when a non-admin user is assigned" do
    user = insert(:user, admin: false)

    conn =
      build_conn()
      |> assign(:user, user)

    result = RequireAdminPlug.call(conn, nil)

    assert redirected_to(result) == "/login"
  end

  test "skips when an admin user is assigned" do
    user = insert(:user, admin: true)

    conn =
      build_conn()
      |> assign(:user, user)

    result = RequireAdminPlug.call(conn, nil)

    assert result == conn
  end
end
