defmodule TribesWeb.Plugs.CookieAuthPlugTest do
  use TribesWeb.ConnCase
  use ExUnit.Case, async: true
  use Plug.Test
  alias Tribes.User
  alias TribesWeb.Plugs.CookieAuthPlug
  import Tribes.Factory

  @session_opts [
    store: :cookie,
    key: "_test",
    signing_salt: "cooldude"
  ]

  test "default behavior" do
    # Create a test connection
    conn =
      conn(:get, "/")
      |> Plug.Session.call(Plug.Session.init(@session_opts))
      |> fetch_session()
      |> CookieAuthPlug.call(nil)

    # Assert the response and status
    assert conn.state == :unset
    assert conn.status == nil
  end

  test "assigns a user when logged in" do
    %{user: user} = session = insert(:session)

    # Create a test connection
    conn =
      conn(:get, "/")
      |> Plug.Session.call(Plug.Session.init(@session_opts))
      |> fetch_session()
      |> put_session(:session_token, session.token)
      |> CookieAuthPlug.call(nil)

    # Assert the response and status
    assert conn.state == :unset
    assert conn.status == nil

    assert %{assigns: %{user: %User{} = ^user}} = conn
  end
end
