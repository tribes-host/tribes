defmodule TribesWeb.Plugs.RequireLoginPlugTest do
  use TribesWeb.ConnCase
  use ExUnit.Case, async: true
  use Plug.Test
  alias TribesWeb.Plugs.RequireLoginPlug
  import Tribes.Factory

  test "redirects to login when no user is assigned" do
    conn = build_conn()
    result = RequireLoginPlug.call(conn, nil)
    assert redirected_to(result) == "/login"
  end

  test "skips when a user is assigned" do
    user = insert(:user)

    conn =
      build_conn()
      |> assign(:user, user)

    result = RequireLoginPlug.call(conn, nil)

    assert result == conn
  end
end
