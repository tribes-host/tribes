defmodule Tribes.ScraperTest do
  use Tribes.DataCase
  alias Tribes.Repo
  alias Tribes.Scraper
  import Tribes.Factory

  test "Scraper.scrape_instance/1" do
    instance =
      insert(:instance,
        domain: "gleasonator.com",
        admin_nickname: "alex",
        feed_type: "local",
        featured_user_nicknames: ["dave"]
      )

    Scraper.scrape_instance(instance)
    instance = Repo.reload(instance)

    assert instance.remote_data["instance"] ==
             json_file("test/fixtures/gleasonator-instance.json")

    assert instance.remote_data["featured_users"] == [
             json_file("test/fixtures/gleasonator-dave.json")
           ]

    assert instance.remote_data["admin"] == json_file("test/fixtures/gleasonator-alex.json")
    assert instance.remote_data["feed"] == json_file("test/fixtures/gleasonator-timeline.json")
  end

  # Read a JSON file as a Map
  defp json_file(filename) do
    filename
    |> File.read!()
    |> Jason.decode!()
  end
end
