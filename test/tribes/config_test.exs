# Modified from Pleroma, test/pleroma/config_test.exs
# Copyright © 2017-2020 Pleroma Authors <https://pleroma.social/>
# SPDX-License-Identifier: AGPL-3.0-only

defmodule Tribes.ConfigTest do
  use ExUnit.Case

  test "get/1 with an atom" do
    assert Tribes.Config.get(:ecto_repos) == Application.get_env(:tribes, :ecto_repos)
    assert Tribes.Config.get(:azertyuiop) == nil
    assert Tribes.Config.get(:azertyuiop, true) == true
  end

  test "get/1 with a list of keys" do
    assert Tribes.Config.get([TribesWeb.Endpoint, :url]) ==
             Keyword.get(Application.get_env(:tribes, TribesWeb.Endpoint), :url)

    assert Tribes.Config.get([TribesWeb.Endpoint, :render_errors, :view]) ==
             get_in(
               Application.get_env(
                 :tribes,
                 TribesWeb.Endpoint
               ),
               [:render_errors, :view]
             )

    assert Tribes.Config.get([:azerty, :uiop]) == nil
    assert Tribes.Config.get([:azerty, :uiop], true) == true
  end

  describe "nil values" do
    setup do
      Tribes.Config.put(:lorem, nil)
      Tribes.Config.put(:ipsum, %{dolor: [sit: nil]})
      Tribes.Config.put(:dolor, sit: %{amet: nil})

      on_exit(fn -> Enum.each(~w(lorem ipsum dolor)a, &Tribes.Config.delete/1) end)
    end

    test "get/1 with an atom for nil value" do
      assert Tribes.Config.get(:lorem) == nil
    end

    test "get/2 with an atom for nil value" do
      assert Tribes.Config.get(:lorem, true) == nil
    end

    test "get/1 with a list of keys for nil value" do
      assert Tribes.Config.get([:ipsum, :dolor, :sit]) == nil
      assert Tribes.Config.get([:dolor, :sit, :amet]) == nil
    end

    test "get/2 with a list of keys for nil value" do
      assert Tribes.Config.get([:ipsum, :dolor, :sit], true) == nil
      assert Tribes.Config.get([:dolor, :sit, :amet], true) == nil
    end
  end

  test "get/1 when value is false" do
    Tribes.Config.put([:instance, :false_test], false)
    Tribes.Config.put([:instance, :nested], [])
    Tribes.Config.put([:instance, :nested, :false_test], false)

    assert Tribes.Config.get([:instance, :false_test]) == false
    assert Tribes.Config.get([:instance, :nested, :false_test]) == false
  end

  test "get!/1" do
    assert Tribes.Config.get!(:ecto_repos) == Application.get_env(:tribes, :ecto_repos)

    assert Tribes.Config.get!([TribesWeb.Endpoint, :url]) ==
             Keyword.get(Application.get_env(:tribes, TribesWeb.Endpoint), :url)

    assert_raise(Tribes.Config.Error, fn ->
      Tribes.Config.get!(:azertyuiop)
    end)

    assert_raise(Tribes.Config.Error, fn ->
      Tribes.Config.get!([:azerty, :uiop])
    end)
  end

  test "get!/1 when value is false" do
    Tribes.Config.put([:instance, :false_test], false)
    Tribes.Config.put([:instance, :nested], [])
    Tribes.Config.put([:instance, :nested, :false_test], false)

    assert Tribes.Config.get!([:instance, :false_test]) == false
    assert Tribes.Config.get!([:instance, :nested, :false_test]) == false
  end

  test "put/2 with a key" do
    Tribes.Config.put(:config_test, true)

    assert Tribes.Config.get(:config_test) == true
  end

  test "put/2 with a list of keys" do
    Tribes.Config.put([:instance, :config_test], true)
    Tribes.Config.put([:instance, :config_nested_test], [])
    Tribes.Config.put([:instance, :config_nested_test, :x], true)

    assert Tribes.Config.get([:instance, :config_test]) == true
    assert Tribes.Config.get([:instance, :config_nested_test, :x]) == true
  end

  test "delete/1 with a key" do
    Tribes.Config.put([:delete_me], :delete_me)
    Tribes.Config.delete([:delete_me])
    assert Tribes.Config.get([:delete_me]) == nil
  end

  test "delete/2 with a list of keys" do
    Tribes.Config.put([:delete_me], hello: "world", world: "Hello")
    Tribes.Config.delete([:delete_me, :world])
    assert Tribes.Config.get([:delete_me]) == [hello: "world"]
    Tribes.Config.put([:delete_me, :delete_me], hello: "world", world: "Hello")
    Tribes.Config.delete([:delete_me, :delete_me, :world])
    assert Tribes.Config.get([:delete_me, :delete_me]) == [hello: "world"]

    assert Tribes.Config.delete([:this_key_does_not_exist])
    assert Tribes.Config.delete([:non, :existing, :key])
  end

  test "fetch/1" do
    Tribes.Config.put([:lorem], :ipsum)
    Tribes.Config.put([:ipsum], dolor: :sit)

    assert Tribes.Config.fetch([:lorem]) == {:ok, :ipsum}
    assert Tribes.Config.fetch(:lorem) == {:ok, :ipsum}
    assert Tribes.Config.fetch([:ipsum, :dolor]) == {:ok, :sit}
    assert Tribes.Config.fetch([:lorem, :ipsum]) == :error
    assert Tribes.Config.fetch([:loremipsum]) == :error
    assert Tribes.Config.fetch(:loremipsum) == :error

    Tribes.Config.delete([:lorem])
    Tribes.Config.delete([:ipsum])
  end
end
