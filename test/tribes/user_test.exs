defmodule Tribes.UserTest do
  use Tribes.DataCase
  alias Tribes.User
  alias TribesWeb.Session
  import Swoosh.TestAssertions
  import Tribes.Factory

  test "get_by_id/1" do
    %User{id: id} = user = insert(:user)
    assert User.get_by_id(id) == user
  end

  test "get_by_email/1" do
    user = insert(:user, email: "hello@world.com")
    assert User.get_by_email("hello@world.com") == user
  end

  test "register/1" do
    {:ok, %Session{user: user}} =
      User.register(%{
        "email" => "hello@world.com",
        "password" => "hello world",
        "accepts_marketing_emails" => "true"
      })

    assert user.email == "hello@world.com"
    assert user.accepts_marketing_emails == true
    assert Pbkdf2.verify_pass("hello world", user.password_hash)
    assert_email_sent(Tribes.UserEmail.verification_email(user))
  end

  test "verify_email/1" do
    user = insert(:user, email_verified: false, email_verification_token: "12345")
    {:ok, user} = User.verify_email(user)

    assert user.email_verified
    refute user.email_verification_token
  end
end
