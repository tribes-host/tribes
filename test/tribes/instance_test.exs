defmodule Tribes.InstanceTest do
  alias Tribes.Repo
  alias Tribes.Instance
  alias TribesPay.Subscription
  use Tribes.DataCase
  import Tribes.Factory

  test "get_by_id/1" do
    %Instance{id: id} = instance = insert(:instance)
    assert Instance.get_by_id(id) == instance
  end

  test "to_k8s_namespace/1" do
    result =
      %Instance{id: "A1KgDe5a6EUPjLp8V6", domain: "yolo.social"}
      |> Instance.to_k8s_namespace()

    assert result == "tribe-cfejlkeefscpyedxafbzqqi"
  end

  test "set_status/2" do
    instance = insert(:instance)
    assert instance.status == :draft
    {:ok, instance} = Instance.set_status(instance, :online)
    assert instance.status == :online
  end

  test "create/3" do
    %Subscription{user_id: user_id} = subscription = insert(:subscription)

    assert {:ok, %Instance{domain: "hello.test", user_id: ^user_id}} =
             Instance.create(subscription, "hello.test")
  end

  test "get_by_domain/1 returns an online instance" do
    user = insert(:user)
    instance = insert(:instance, user_id: user.id, domain: "gleasonator.com", status: :online)
    insert(:instance, domain: "gleasonator.com", status: :draft)

    assert instance == Instance.get_by_domain("gleasonator.com")
  end

  test "get_by_domain/2 returns the user's instance" do
    user = insert(:user)
    instance = insert(:instance, user_id: user.id, domain: "gleasonator.com", status: :draft)
    insert(:instance, domain: "gleasonator.com", status: :online)

    assert instance == Instance.get_by_domain(user, "gleasonator.com")
  end

  test "create instance with valid tags" do
    attrs = %{
      domain: "gleasonator.com",
      tags: ["interest:technology", "activism:foss"]
    }

    {:ok, %Instance{} = instance} =
      Instance.changeset(%Instance{}, attrs)
      |> Repo.insert()

    assert instance.tags == ["interest:technology", "activism:foss"]
  end

  test "create instance with invalid tags" do
    attrs = %{
      domain: "gleasonator.com",
      tags: ["interest:benis", "activism:socialmedia"]
    }

    {:error, %Ecto.Changeset{} = _instance} =
      Instance.changeset(%Instance{}, attrs)
      |> Repo.insert()
  end

  test "filter by tags" do
    instance =
      insert(:instance, domain: "gleasonator.com", tags: ["interest:technology", "activism:foss"])

    insert(:instance, domain: "spinster.xyz", tags: ["activism:feminist", "interest:politics"])

    [^instance] = Instance.filter(["activism:foss", "interest:art"])

    instance2 = insert(:instance, domain: "neenster.org", tags: ["interest:art"])

    [^instance, ^instance2] = Instance.filter(["activism:foss", "interest:art"])

    assert Enum.count(Instance.filter([])) == 3
  end

  test "domain uniqueness" do
    user = insert(:user)
    user2 = insert(:user)

    assert insert(:instance, user_id: user.id, domain: "hello.com")

    assert insert(:instance, user_id: user2.id, domain: "hello.com")

    assert insert(:instance, user_id: user.id, domain: "yolo.space", status: :online)

    assert_raise(Ecto.ConstraintError, fn ->
      insert(:instance, user_id: user2.id, domain: "yolo.space", status: :deploying)
    end)

    assert_raise(Ecto.ConstraintError, fn ->
      insert(:instance, user_id: user2.id, domain: "yolo.space", status: :online)
    end)
  end
end
