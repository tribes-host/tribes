# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Tribes.Repo.insert!(%Tribes.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

Tribes.Repo.insert!(%Tribes.User{
  email: "test@tribes.host",
  password_hash: Pbkdf2.hash_pwd_salt("testing123")
})

Tribes.Repo.insert!(%Tribes.Instance{
  domain: "gleasonator.com",
  tags: ["interest:technology", "activism:foss"],
  admin_nickname: "alex",
  feed_type: "local",
  featured_user_nicknames: ["dave"]
})

Tribes.Repo.insert!(%Tribes.Instance{
  domain: "spinster.xyz",
  tags: ["activism:feminist", "interest:politics"],
  admin_nickname: "mk",
  feed_type: "user",
  feed_user_nickname: "spinster",
  featured_user_nicknames: ["meghanmurphy", "ninapaley"]
})

Tribes.Repo.insert!(%Tribes.Instance{
  domain: "neenster.org",
  tags: ["interest:art"],
  admin_nickname: "nina",
  feed_type: "local",
  featured_user_nicknames: ["kfogel", "mittimithai"]
})

Tribes.Repo.insert!(%Tribes.Instance{
  domain: "glindr.org",
  admin_nickname: "1202",
  feed_user_nickname: "1202",
  tags: ["interest:politics"],
  featured_user_nicknames: ["2623"]
})

Tribes.Repo.insert!(%Tribes.Instance{
  domain: "animalliberation.social",
  tags: ["activism:vegan"],
  admin_nickname: "1",
  feed_type: "local"
})
