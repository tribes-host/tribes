defmodule Tribes.Repo.Migrations.AddInstanceProfileFields do
  use Ecto.Migration

  def change do
    alter table(:instances) do
      add :feed_acct, :string
      add :admin_acct, :string
      add :featured_members, {:array, :string}
      add :feed_type, :string
    end
  end
end
