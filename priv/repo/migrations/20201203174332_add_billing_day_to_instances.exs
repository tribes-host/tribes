defmodule Tribes.Repo.Migrations.AddBillingDayToInstances do
  use Ecto.Migration

  def change do
    alter table(:instances) do
      add :billing_day, :integer
    end
  end
end
