defmodule Tribes.Repo.Migrations.AddTierToInstances do
  use Ecto.Migration

  def up do
    alter table(:instances) do
      add :tier, :string, default: "small", null: false
    end

    execute "ALTER TABLE instances ALTER COLUMN tier DROP DEFAULT"
  end

  def down do
    alter table(:instances) do
      remove :tier
    end
  end
end
