defmodule Tribes.Repo.Migrations.AddStatusToInstances do
  use Ecto.Migration

  def change do
    alter table(:instances) do
      # Can be "draft", "deploying", "online", "destroying", or "offline"
      add :status, :string, null: false, default: "draft"
    end
  end
end
