defmodule Tribes.Repo.Migrations.CreateInstances do
  use Ecto.Migration

  def change do
    create table(:instances) do
      add :name, :string
      add :domain, :string

      timestamps()
    end

    create unique_index(:instances, [:domain])
  end
end
