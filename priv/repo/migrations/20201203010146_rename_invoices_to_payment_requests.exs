defmodule Tribes.Repo.Migrations.RenameInvoicesToPaymentRequests do
  use Ecto.Migration

  def change do
    rename table(:invoices), to: table(:payment_requests)
  end
end
