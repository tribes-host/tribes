defmodule Tribes.Repo.Migrations.AddMarketingToUsers do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :accepts_marketing_emails, :boolean, null: false, default: false
    end
  end
end
