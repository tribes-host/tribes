defmodule Tribes.Repo.Migrations.DropUniqueInstanceIndex do
  use Ecto.Migration

  def change do
    drop unique_index(:instances, [:owner_id, :domain])
  end
end
