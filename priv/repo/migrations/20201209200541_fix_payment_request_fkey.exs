defmodule Tribes.Repo.Migrations.FixPaymentRequestFkey do
  use Ecto.Migration

  def change do
    alter table(:payments) do
      remove :payment_request_id
      add :payment_request_id, references(:payment_requests, type: :uuid), null: false
    end
  end
end
