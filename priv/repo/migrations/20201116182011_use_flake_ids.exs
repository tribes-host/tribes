defmodule Tribes.Repo.Migrations.UseFlakeIds do
  use Ecto.Migration

  # Nuking the tables just seems easier at this point tbh
  def change do
    drop table(:users)
    drop table(:instances)

    create table(:users, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :email, :string, null: false
      add :password_hash, :string, null: false

      timestamps()
    end

    create table(:instances, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :domain, :string, null: false
      add :tags, {:array, :string}, default: []
      add :remote_data, :map, default: %{}
      add :feed_type, :string, default: "local"
      add :feed_user_nickname, :string
      add :admin_nickname, :string
      add :featured_user_nicknames, {:array, :string}, default: []

      timestamps()
    end

    create unique_index(:users, [:email])
    create unique_index(:instances, [:domain])
  end
end
