defmodule Tribes.Repo.Migrations.AddPaymentTables do
  use Ecto.Migration

  def change do
    create table(:invoices, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :payer_id, references(:users, type: :uuid), null: false

      # "open", "paid"
      add :status, :string, null: false

      # "tribe", "patron"
      add :product, :string, null: false

      # eg domain, month, etc
      add :product_data, :map

      add :amount, :integer, null: false
      add :currency, :string, null: false

      timestamps()
    end

    create table(:payments, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :payer_id, references(:users, type: :uuid), null: false
      add :invoice_id, references(:invoices, type: :uuid), null: false

      add :amount_usd, :integer, null: false
      add :source_amount, :integer, null: false
      add :source_currency, :string, null: false

      # "authorizenet", "btcpay"
      add :gateway, :string, null: false

      # eg token, last4, etc
      add :gateway_data, :map

      timestamps()
    end

    create table(:payment_methods, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :payer_id, references(:users, type: :uuid), null: false

      # "authorizenet", "btcpay"
      add :gateway, :string, null: false

      # eg token, last4, etc
      add :gateway_data, :map

      timestamps()
    end
  end
end
