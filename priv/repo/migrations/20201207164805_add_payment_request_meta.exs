defmodule Tribes.Repo.Migrations.AddPaymentRequestMeta do
  use Ecto.Migration

  def change do
    create table(:payment_request_meta, primary_key: false) do
      add :payment_request_id, references(:payment_requests, type: :uuid), null: false
      add :key, :string, null: false
      add :value, :string, null: false
    end

    create unique_index(:payment_request_meta, [:payment_request_id, :key])
  end
end
