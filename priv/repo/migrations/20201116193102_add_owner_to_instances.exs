defmodule Tribes.Repo.Migrations.AddOwnerToInstances do
  use Ecto.Migration

  def change do
    alter table(:instances) do
      add :owner_id, references(:users, type: :uuid)
    end
  end
end
