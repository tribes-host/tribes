defmodule Tribes.Repo.Migrations.AddBannerToInstances do
  use Ecto.Migration

  def change do
    alter table(:instances) do
      add :banner, :string
    end
  end
end
