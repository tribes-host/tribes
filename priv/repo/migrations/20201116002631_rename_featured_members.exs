defmodule Tribes.Repo.Migrations.RenameFeaturedMembers do
  use Ecto.Migration

  def change do
    rename table(:instances), :featured_member_nicknames, to: :featured_user_nicknames
  end
end
