defmodule Tribes.Repo.Migrations.CreateSubscriptionsTable do
  use Ecto.Migration

  def change do
    create table(:subscriptions, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :user_id, references(:users, type: :uuid), null: false
      add :status, :string, null: false
      add :tier, :string, null: false
      add :billing_day, :integer, null: false

      timestamps()
    end

    alter table(:instances) do
      remove :tier, :string
      remove :billing_day, :integer
      add :subscription_id, references(:subscriptions, type: :uuid)
    end

    execute "TRUNCATE TABLE payment_requests, payments"

    alter table(:payment_requests) do
      add :subscription_id, references(:subscriptions, type: :uuid), null: false
      add :billing_period, :string, null: false
    end
  end
end
