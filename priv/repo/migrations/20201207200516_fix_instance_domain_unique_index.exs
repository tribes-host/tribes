defmodule Tribes.Repo.Migrations.FixInstanceDomainUniqueIndex do
  use Ecto.Migration

  def change do
    drop unique_index(:instances, [:domain])
    create unique_index(:instances, [:owner_id, :domain])
    create unique_index(:instances, [:domain], where: "status != 'draft'")
  end
end
