defmodule Tribes.Repo.Migrations.AddEmailVerifiedToUsers do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :email_verified, :boolean, null: false, default: false
    end
  end
end
