defmodule Tribes.Repo.Migrations.PaymentRequestDefaultStatus do
  use Ecto.Migration

  def change do
    alter table(:payment_requests) do
      modify :status, :string, null: false, default: "unpaid"
    end
  end
end
