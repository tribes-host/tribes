defmodule Tribes.Repo.Migrations.AddEmailVerificationTokenToUsers do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :email_verification_token, :string, default: nil
    end
  end
end
