defmodule Tribes.Repo.Migrations.RenamePaymentFields do
  use Ecto.Migration

  def change do
    rename table(:payments), :invoice_id, to: :payment_request_id
    rename table(:payments), :source_amount, to: :amount
    rename table(:payments), :source_currency, to: :currency
  end
end
