defmodule Tribes.Repo.Migrations.AddUserTags do
  use Ecto.Migration

  def change do
    alter table(:instances) do
      add :tags, {:array, :string}
    end
  end
end
