defmodule Tribes.Repo.Migrations.InstanceServerapiUpdates do
  use Ecto.Migration

  def change do
    alter table(:instances) do
      add :data, :map
      add :account_data, :map
      remove :name, :string
      remove :banner, :string
    end
  end
end
