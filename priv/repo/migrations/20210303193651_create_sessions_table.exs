defmodule Tribes.Repo.Migrations.CreateSessionsTable do
  use Ecto.Migration

  def change do
    create_if_not_exists table(:sessions) do
      add :user_id, references(:users, type: :uuid), null: false
      add :token, :string, null: false

      timestamps()
    end

    create unique_index(:sessions, [:token])
  end
end
