defmodule Tribes.Repo.Migrations.RenameInstanceFields do
  use Ecto.Migration

  def change do
    rename table(:instances), :data, to: :remote_data
    rename table(:instances), :feed_acct, to: :feed_user_nickname
    rename table(:instances), :admin_acct, to: :admin_nickname
    rename table(:instances), :featured_members, to: :featured_member_nicknames

    alter table(:instances) do
      remove :account_data, :map, default: %{}
    end
  end
end
