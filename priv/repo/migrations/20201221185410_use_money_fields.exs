defmodule Tribes.Repo.Migrations.UseMoneyFields do
  use Ecto.Migration

  def up do
    # It's okay to be destructive at this stage
    execute "TRUNCATE TABLE payment_requests, payments, payment_request_meta"

    alter table(:payment_requests) do
      remove :amount, :integer, null: false
      remove :currency, :string, null: false
      add :price, :money_with_currency, null: false
    end

    alter table(:payments) do
      remove :amount, :integer, null: false
      remove :currency, :string, null: false
      add :price, :money_with_currency, null: false
    end
  end

  def down do
    alter table(:payment_requests) do
      remove :price, :money_with_currency, null: false
      add :amount, :integer, null: false
      add :currency, :string, null: false
    end

    alter table(:payments) do
      remove :price, :money_with_currency, null: false
      add :amount, :integer, null: false
      add :currency, :string, null: false
    end
  end
end
