defmodule Tribes.Repo.Migrations.SimplifyPaymentTables do
  use Ecto.Migration

  def change do
    drop table(:payment_request_meta)
    drop table(:payment_methods)

    rename table(:payments), :payer_id, to: :user_id

    alter table(:payments) do
      remove :amount_usd, :integer, null: false
      add :amount_converted, :money_with_currency, null: false
    end

    rename table(:payment_requests), :payer_id, to: :user_id

    alter table(:payment_requests) do
      remove :product, :string, null: false
      remove :product_data, :map
    end

    rename table(:instances), :owner_id, to: :user_id
  end
end
