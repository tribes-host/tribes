defmodule Tribes.Repo.Migrations.BtcpayUniqueWebhookDelivery do
  use Ecto.Migration

  def change do
    create unique_index(:payments, ["(gateway_data->>'orignalDeliveryId')"],
             where: "gateway = 'btcpay'"
           )
  end
end
