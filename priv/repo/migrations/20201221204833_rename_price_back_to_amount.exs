defmodule Tribes.Repo.Migrations.RenamePriceBackToAmount do
  use Ecto.Migration

  def change do
    rename table(:payments), :price, to: :amount
    rename table(:payment_requests), :price, to: :amount
  end
end
