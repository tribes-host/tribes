defmodule Tribes.Repo.Migrations.AddPasswordResetTokenToUsers do
  use Ecto.Migration

  def change do
    alter table(:users) do
      add :password_reset_token, :string
      add :password_reset_token_emailed_at, :naive_datetime
      add :email_verification_token_emailed_at, :naive_datetime
    end
  end
end
