defmodule Tribes.User do
  @moduledoc """
  The end-user that can log in and use Tribes.
  """
  use Ecto.Schema
  alias Tribes.Repo
  alias Tribes.User
  alias TribesWeb.Session
  import Ecto.Changeset

  @primary_key {:id, FlakeId.Ecto.Type, autogenerate: true}

  # credo:disable-for-next-line Credo.Check.Readability.MaxLineLength
  @email_regex ~r/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/

  schema "users" do
    field :admin, :boolean, null: false, default: false
    field :email, :string, null: false
    field :email_verified, :boolean, null: false, default: false
    field :email_verification_token, :string
    field :email_verification_token_emailed_at, :naive_datetime
    field :password_reset_token, :string
    field :password_reset_token_emailed_at, :naive_datetime
    field :password_hash, :string, null: false
    field :accepts_marketing_emails, :boolean, null: false, default: false

    timestamps()
  end

  def validation_changeset(changeset) do
    changeset
    |> validate_format(:email, @email_regex)
    |> validate_required([:email, :password_hash])
    |> unique_constraint(:email)
  end

  def get_by_id(id) when is_binary(id) do
    Repo.get_by(User, id: id)
  end

  def get_by_id(_id), do: nil

  def get_by_email(email) do
    Repo.get_by(User, email: email)
  end

  def check_pass(%User{} = user, password) do
    Pbkdf2.check_pass(user, password)
  end

  def email_verification_changeset(changeset) do
    now = get_naive_timestamp()
    token = generate_token()

    changeset
    |> change(email_verified: false)
    |> change(email_verification_token: token)
    |> change(email_verification_token_emailed_at: now)
  end

  def password_reset_changeset(changeset) do
    now = get_naive_timestamp()
    token = generate_token()

    changeset
    |> change(password_reset_token: token)
    |> change(password_reset_token_emailed_at: now)
  end

  def verify_email(%User{} = user) do
    user
    |> change(email_verified: true, email_verification_token: nil)
    |> Repo.update()
  end

  def send_password_reset(%User{} = user) do
    changeset = password_reset_changeset(user)

    with {:ok, %User{} = user} <- Repo.update(changeset) do
      user
      |> Tribes.UserEmail.password_reset_email()
      |> Tribes.Mailer.deliver()

      {:ok, user}
    end
  end

  def change_password(%User{} = user, password) do
    user
    |> put_password_hash(password)
    |> Repo.update()
  end

  defp put_password_hash(changeset, password) when is_binary(password) do
    change(changeset, Pbkdf2.add_hash(password))
  end

  def register_changeset(params) do
    password = params[:password] || params["password"]

    %User{}
    |> cast(params, [:email, :accepts_marketing_emails])
    |> put_password_hash(password)
    |> email_verification_changeset()
  end

  def register(params) do
    changeset = register_changeset(params)

    with {:ok, %User{} = user} <- create(changeset),
         {:ok, %Session{} = session} <- Session.create(user) do
      user
      |> Tribes.UserEmail.verification_email()
      |> Tribes.Mailer.deliver()

      {:ok, Repo.preload(session, :user)}
    end
  end

  def create(changeset) do
    changeset
    |> validation_changeset()
    |> Repo.insert()
  end

  defp generate_token(), do: :crypto.strong_rand_bytes(32) |> Base.url_encode64()

  defp get_naive_timestamp(), do: NaiveDateTime.truncate(NaiveDateTime.utc_now(), :second)

  def make_admin(%User{admin: true} = user), do: user

  def make_admin(%User{} = user) do
    user
    |> change(admin: true)
    |> Repo.update()
  end
end
