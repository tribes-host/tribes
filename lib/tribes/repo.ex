defmodule Tribes.Repo do
  @moduledoc """
  All PostgreSQL interactions occur through the Repo.
  """
  use Ecto.Repo,
    otp_app: :tribes,
    adapter: Ecto.Adapters.Postgres
end
