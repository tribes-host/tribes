defmodule Tribes.Instance do
  @moduledoc """
  Represents a deployment that is, was, or will be.
  """
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query
  alias Tribes.Instance
  alias Tribes.Repo
  alias Tribes.User
  alias TribesPay.Payment
  alias TribesPay.PaymentRequest
  alias TribesPay.Subscription

  @primary_key {:id, FlakeId.Ecto.Type, autogenerate: true}

  @domain_regex ~r/^[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/

  @valid_tags [
    "interest:art",
    "interest:science",
    "interest:technology",
    "interest:politics",
    "interest:gaming",
    "interest:occult",
    "interest:comedy",
    "activism:vegan",
    "activism:feminist",
    "activism:foss",
    "activism:religion"
  ]

  @derive {Jason.Encoder, only: [:domain, :tags, :remote_data]}
  schema "instances" do
    belongs_to(:user, User, type: FlakeId.Ecto.CompatType)
    belongs_to(:subscription, Subscription, type: FlakeId.Ecto.CompatType)

    field :status, Ecto.Enum,
      values: [:draft, :deploying, :online, :destroying],
      null: false,
      default: :draft

    field :domain, :string, null: false
    field :tags, {:array, :string}, default: []

    field :remote_data, :map, default: %{}

    field :feed_type, :string, default: "local"
    field :feed_user_nickname, :string

    field :admin_nickname, :string
    field :featured_user_nicknames, {:array, :string}, default: []

    timestamps()
  end

  @doc false
  def changeset(instance, attrs) do
    instance
    |> cast(attrs, [
      :domain,
      :tags,
      :remote_data,
      :feed_type,
      :feed_user_nickname,
      :admin_nickname,
      :featured_user_nicknames
    ])
    |> validate_required([:domain])
    |> validate_tags()
  end

  def create(%Subscription{id: subscription_id, user_id: user_id}, domain)
      when is_binary(domain) do
    %Instance{}
    |> change(user_id: user_id, subscription_id: subscription_id, domain: domain)
    |> validate_format(:domain, @domain_regex)
    |> Repo.insert()
  end

  def delete(%Instance{} = instance), do: Repo.delete(instance)

  def validate_tags(changeset) do
    validate_change(changeset, :tags, fn :tags, tags ->
      if Enum.all?(tags, fn tag -> Enum.member?(@valid_tags, tag) end) do
        []
      else
        [tags: "invalid"]
      end
    end)
  end

  def get_by_id(id) when is_binary(id) do
    Repo.get(Instance, id)
  end

  def to_k8s_namespace(%Instance{id: id}) do
    codename =
      id
      |> FlakeId.from_string()
      |> FlakeId.to_integer()
      |> TribesDeploy.Base26.encode()

    "tribe-#{codename}"
  end

  def get_by_domain(domain) when is_binary(domain) do
    Instance
    |> where(domain: ^domain, status: :online)
    |> Repo.one()
  end

  def get_by_domain(%User{id: user_id}, domain) when is_binary(domain) do
    Instance
    |> where(domain: ^domain, user_id: ^user_id)
    |> Repo.all()
    |> case do
      # HACK: https://gitlab.com/tribes-host/tribes/-/issues/60
      [%Instance{} = instance] ->
        instance

      instances when is_list(instances) ->
        instances
        |> Enum.find(fn instance ->
          instance.status in [:deploying, :online]
        end)
    end
  end

  def get_for_user(%User{id: user_id} = _user, statuses) when is_list(statuses) do
    Instance
    |> where(user_id: ^user_id)
    |> where([i], i.status in ^statuses)
    |> Repo.all()
  end

  def get_for_user(%User{} = user, status) when is_atom(status) do
    get_for_user(user, [status])
  end

  def get_for_user(%User{id: user_id} = _user) do
    Instance
    |> where(user_id: ^user_id)
    |> Repo.all()
  end

  def get_by_subscription_id(id) when is_binary(id) do
    Instance
    |> where(subscription_id: ^id)
    |> Repo.one()
  end

  def set_status(%Instance{} = instance, status) do
    instance
    |> change(status: status)
    |> Repo.update()
  end

  def filter([]) do
    Repo.all(Instance)
  end

  def filter(tags) when is_list(tags) do
    Instance
    |> where([i], fragment("? && ?", i.tags, ^tags))
    |> Repo.all()
  end

  def get_url(%Instance{domain: domain}) do
    "https://#{domain}"
  end

  def get_subscription_status(%Instance{subscription_id: nil}), do: :free

  def get_subscription_status(%Instance{subscription: %Subscription{id: id}}) do
    PaymentRequest
    |> where(subscription_id: ^id)
    |> where(status: :unpaid)
    |> Repo.all()
    |> case do
      [%PaymentRequest{} = pr] -> {:unpaid, pr}
      nil -> :paid
      other -> {:unpaid, other}
    end
  end

  def get_subscription_status(%Instance{} = instance) do
    instance
    |> Repo.preload(:subscription)
    |> get_subscription_status()
  end

  def get_payments(%Instance{subscription_id: nil}), do: []

  def get_payments(%Instance{subscription: %Subscription{id: id}}) do
    Payment
    |> join(:left, [p], pr in PaymentRequest, on: p.payment_request_id == pr.id)
    |> where([p, pr], pr.subscription_id == ^id)
    |> Repo.all()
  end

  def get_payments(%Instance{} = instance) do
    instance
    |> Repo.preload(:subscription)
    |> get_payments()
  end

  def cancel_subscription(%Instance{subscription_id: nil}), do: {:ok, nil}

  def cancel_subscription(%Instance{
        subscription: %Subscription{status: :cancelled} = subscription
      }),
      do: {:ok, subscription}

  def cancel_subscription(%Instance{subscription: %Subscription{} = subscription}) do
    subscription
    |> change(status: :cancelled)
    |> Repo.update()
  end

  def cancel_subscription(%Instance{} = instance) do
    instance
    |> Repo.preload(:subscription)
    |> cancel_subscription()
  end
end
