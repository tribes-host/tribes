defmodule Tribes.UserEmail do
  @moduledoc """
  A collection of emails relating to user accounts, such as email verification
  and "I forgot my password".
  """
  import Swoosh.Email
  alias Tribes.Instance
  alias Tribes.User
  alias TribesPay.Payment
  alias TribesWeb.Endpoint
  alias TribesWeb.Router

  def verification_email(%User{id: user_id, email: email, email_verification_token: token}) do
    verification_url = Router.Helpers.auth_url(Endpoint, :verify_email, user_id, to_string(token))

    body = """
    Hello,

    Please confirm your account: #{verification_url}

    Thanks!
    Tribes.host
    """

    new()
    |> to(email)
    |> from({"Tribes.host", Tribes.Config.get(:notification_email)})
    |> subject("Tribes: Confirm your account")
    |> text_body(body)
  end

  def password_reset_email(%User{id: user_id, email: email, password_reset_token: token}) do
    password_reset_url =
      Router.Helpers.auth_url(Endpoint, :password_reset_form, user_id, to_string(token))

    body = """
    Hello,

    You may reset your password here: #{password_reset_url}

    Tribes.host
    """

    new()
    |> to(email)
    |> from({"Tribes.host", Tribes.Config.get(:notification_email)})
    |> subject("Tribes: Reset your password")
    |> text_body(body)
  end

  def instance_deploying_email(%User{email: email}, %Instance{domain: domain}) do
    body = """
    Thanks for your payment!

    Your tribe at #{domain} is currently being deployed.
    Please allow up to 15 minutes for this process to complete.
    You will receive another email when it's deployed.

    If you encounter problems, you can contact us at: #{Tribes.Config.get(:support_email)}

    Please ensure your DNS is set correctly:
    #{domain}. CNAME #{Tribes.Config.get(:cname)}

    If the deploy fails, we will automatically retry it for up to a month.

    Thanks!
    Tribes.host
    """

    new()
    |> to(email)
    |> from({"Tribes.host", Tribes.Config.get(:notification_email)})
    |> subject("[#{domain}] Payment successful")
    |> text_body(body)
  end

  def instance_ready_email(%User{email: email}, %Instance{domain: domain}, password) do
    body = """
    Your tribe is ready! Access it from https://#{domain}

    Username: admin
    Password: #{password}

    Tribes.host
    """

    new()
    |> to(email)
    |> from({"Tribes.host", Tribes.Config.get(:notification_email)})
    |> subject("[#{domain}] Your tribe is ready!")
    |> text_body(body)
  end
end
