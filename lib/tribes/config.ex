# Modified from Pleroma, lib/pleroma/config.ex
# Copyright © 2017-2020 Pleroma Authors <https://pleroma.social/>
# SPDX-License-Identifier: AGPL-3.0-only

defmodule Tribes.Config do
  @moduledoc """
  An improved config interface for managing deeply nested values.
  """
  defmodule Error do
    defexception [:message]
  end

  def get(key), do: get(key, nil)

  def get([key], default), do: get(key, default)

  def get([_ | _] = path, default) do
    case fetch(path) do
      {:ok, value} -> value
      :error -> default
    end
  end

  def get(key, default) do
    Application.get_env(:tribes, key, default)
  end

  def get!(key) do
    value = get(key, nil)

    if value == nil do
      raise(Error, message: "Missing configuration value: #{inspect(key)}")
    else
      value
    end
  end

  def fetch(key) when is_atom(key), do: fetch([key])

  def fetch([root_key | keys]) do
    Enum.reduce_while(keys, Application.fetch_env(:tribes, root_key), fn
      key, {:ok, config} when is_map(config) or is_list(config) ->
        case Access.fetch(config, key) do
          :error ->
            {:halt, :error}

          value ->
            {:cont, value}
        end

      _key, _config ->
        {:halt, :error}
    end)
  end

  def put([key], value), do: put(key, value)

  def put([parent_key | keys], value) do
    parent =
      Application.get_env(:tribes, parent_key, [])
      |> put_in(keys, value)

    Application.put_env(:tribes, parent_key, parent)
  end

  def put(key, value) do
    Application.put_env(:tribes, key, value)
  end

  def delete([key]), do: delete(key)

  def delete([parent_key | keys] = path) do
    with {:ok, _} <- fetch(path) do
      {_, parent} =
        parent_key
        |> get()
        |> get_and_update_in(keys, fn _ -> :pop end)

      Application.put_env(:tribes, parent_key, parent)
    end
  end

  def delete(key) do
    Application.delete_env(:tribes, key)
  end
end
