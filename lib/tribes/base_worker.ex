defmodule Tribes.BaseWorker do
  @moduledoc """
  Module other Workers inherit from.
  """
  defmacro __using__(opts) do
    caller_module = __CALLER__.module

    quote do
      use Oban.Worker, unquote(opts)
      alias Oban.Job

      def enqueue(params, worker_args \\ [])

      def enqueue(%Tribes.Instance{id: id, domain: domain, user_id: user_id}, worker_opts) do
        enqueue(
          %{
            instance_id: id,
            domain: domain,
            user_id: user_id
          },
          worker_opts
        )
      end

      def enqueue(params, worker_opts) do
        unquote(caller_module)
        |> apply(:new, [params, worker_opts])
        |> Oban.insert()
      end

      def parse_strict(result) do
        case result do
          :ok -> :ok
          :discard -> :discard
          {:ok, value} -> {:ok, value}
          {:error, reason} -> {:error, reason}
          {:discard, reason} -> {:discard, reason}
          {:snooze, seconds} -> {:snooze, seconds}
          unhandled -> {:error, {:unhandled, unhandled}}
        end
      end
    end
  end
end
