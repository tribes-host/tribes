defmodule Tribes.Mailer do
  @moduledoc """
  Mailer functions with Swoosh.
  """
  use Swoosh.Mailer, otp_app: :tribes
end
