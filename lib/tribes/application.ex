defmodule Tribes.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Ecto repository
      Tribes.Repo,
      # Start the Telemetry supervisor
      TribesWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, name: Tribes.PubSub},
      # Start the Endpoint (http/https)
      TribesWeb.Endpoint,
      # Start a worker by calling: Tribes.Worker.start_link(arg)
      # {Tribes.Worker, arg}
      {Oban, oban_config()}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Tribes.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    TribesWeb.Endpoint.config_change(changed, removed)
    :ok
  end

  # Conditionally disable crontab, queues, or plugins here.
  defp oban_config do
    Application.get_env(:tribes, Oban)
  end
end
