defmodule Tribes.Scraper do
  @moduledoc """
  Scrapes remote instance data for display in the Explore area.
  Most aspects of an instance are configured from the instance itself
  rather than through Tribes, so we need a way to pull it in.
  """
  alias Tribes.Instance
  alias Tribes.Repo

  @http_client Application.get_env(:tribes, Tribes.Scraper)[:http_client]

  def scrape_instance(%Instance{} = instance) do
    types = [:instance, :featured_users, :admin, :feed]
    remote_data = build_remote_data(instance, types)

    instance
    |> Ecto.Changeset.change(remote_data: remote_data)
    |> Repo.update()
  end

  defp build_remote_data(%Instance{} = instance, types) when is_list(types) do
    Enum.reduce(types, %{}, fn type, data ->
      with {:ok, fetched_data} <- fetch_data(type, instance) do
        Map.put(data, type, fetched_data)
      else
        _ -> data
      end
    end)
  end

  def fetch_data(:instance, %Instance{domain: domain}) do
    get_json("https://#{domain}/api/v1/instance")
  end

  def fetch_data(:featured_users, %Instance{featured_user_nicknames: nicknames} = instance)
      when is_list(nicknames) do
    build_accounts_list(instance, nicknames)
  end

  def fetch_data(:admin, %Instance{admin_nickname: nickname} = instance)
      when is_binary(nickname) do
    fetch_account(instance, nickname)
  end

  def fetch_data(:feed, %Instance{domain: domain, feed_type: "user", feed_user_nickname: nickname})
      when is_binary(nickname) do
    get_json(
      "https://#{domain}/api/v1/accounts/#{nickname}/statuses?exclude_replies=true&exclude_reblogs=true"
    )
  end

  def fetch_data(:feed, %Instance{domain: domain, feed_type: "local"}) do
    get_json("https://#{domain}/api/v1/timelines/public?local=true")
  end

  def fetch_data(_type, _instance), do: {:error, "Invalid type or nickname."}

  defp fetch_account(%Instance{domain: domain}, nickname) when is_binary(nickname) do
    get_json("https://#{domain}/api/v1/accounts/#{nickname}")
  end

  defp fetch_account(_instance, _nickname), do: {:error, "Invalid Instance or nickname."}

  defp build_accounts_list(%Instance{domain: _} = instance, nicknames) when is_list(nicknames) do
    result =
      Enum.reduce(nicknames, [], fn nickname, accounts ->
        with {:ok, %{} = account_data} <- fetch_account(instance, nickname) do
          accounts ++ [account_data]
        else
          _ -> accounts
        end
      end)

    {:ok, result}
  end

  defp build_accounts_list(_instance, _nicknames), do: {:error, "Invalid Instance or nicknames."}

  defp get_json(url) do
    with {:ok, res} <- @http_client.get(url),
         %HTTPoison.Response{body: body} <- res,
         {:ok, json} <- Jason.decode(body) do
      {:ok, json}
    end
  end
end
