defmodule TribesDeploy.K8sExec do
  @moduledoc """
  Runs commands on K8s pods.
  """
  alias K8s.Client.Runner.PodExec
  alias Tribes.Instance

  def create_admin_user(%Instance{domain: domain} = instance) do
    password = generate_password()

    exec_opts = [
      command: [
        "/opt/pleroma/bin/pleroma_ctl",
        "user",
        "new",
        "admin",
        "admin@#{domain}",
        "--password #{password}",
        "--admin",
        "--assume-yes"
      ],
      container: "pleroma",
      stdin: true,
      stderr: true,
      stdout: true,
      tty: true,
      stream_to: self()
    ]

    with {:ok, conn} <- K8s.Conn.lookup(:tribes),
         {:ok, pid} <- PodExec.run(get_operation(instance), conn, exec_opts) do
      receive_loop(pid, password)
    end
  end

  defp get_pod(%Instance{} = instance) do
    with {:ok, %{"items" => pods}} <- get_pods(instance) do
      # HACK: just take the first pod since there should only be one
      {:ok, Enum.at(pods, 0)}
    end
  end

  defp get_pods(%Instance{} = instance) do
    with {:ok, conn} <- K8s.Conn.lookup(:tribes) do
      K8s.Client.list("v1", "Pod", namespace: Instance.to_k8s_namespace(instance))
      |> K8s.Client.run(conn)
    end
  end

  defp get_operation(%Instance{} = instance) do
    with {:ok, %{"metadata" => %{"name" => pod_name}}} <- get_pod(instance) do
      K8s.Client.create(
        "v1",
        "pods/exec",
        [namespace: Instance.to_k8s_namespace(instance), name: pod_name],
        nil
      )
    end
  end

  defp receive_loop(pid, data) do
    receive do
      {:ok, _message} -> receive_loop(pid, data)
      {:exit, _} -> {:ok, data}
    after
      60_000 -> Process.exit(pid, :kill)
    end
  end

  defp generate_password() do
    :crypto.strong_rand_bytes(16)
    |> Base.encode64()
    |> String.replace_suffix("==", "")
  end
end
