defmodule TribesDeploy.Base26 do
  @moduledoc """
  Converts values to a custom base26 format using only lowercase
  latin letters. This is needed because K8S resources names are very strict.
  """
  @alphabet [
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z"
  ]

  def encode(val), do: Convertat.to_base(val, @alphabet)

  def decode(val), do: Convertat.from_base(val, @alphabet)
end
