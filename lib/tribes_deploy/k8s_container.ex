defmodule TribesDeploy.K8sContainer do
  @moduledoc """
  Generates K8S container manifests.
  """
  alias Tribes.Instance
  alias TribesDeploy.K8sVolume

  def generate_containers(types, %Instance{} = instance) when is_list(types) do
    Enum.map(types, fn type -> generate_container(type, instance) end)
  end

  def generate_container(:pleroma, %Instance{domain: domain} = instance) do
    %{name: volume_name} = K8sVolume.get_volume_specs(:media, instance)

    %{
      "name" => "pleroma",
      "image" => "tribeshost/tribe:latest",
      "ports" => [
        %{
          "containerPort" => 4000
        }
      ],
      "env" => [
        %{
          "name" => "URL_DOMAIN",
          "value" => domain
        },
        %{
          "name" => "DB_HOST",
          "value" => "localhost"
        },
        %{
          "name" => "DB_PASS",
          "value" => "postgres"
        }
      ],
      "volumeMounts" => [
        %{
          "name" => volume_name,
          "mountPath" => "/var/lib/pleroma/uploads"
        }
      ]
    }
  end

  def generate_container(:postgres, instance) do
    %{name: volume_name} = K8sVolume.get_volume_specs(:db, instance)

    %{
      "name" => "postgres",
      "image" => "postgres:13-alpine",
      "ports" => [
        %{
          "containerPort" => 5432
        }
      ],
      "env" => [
        %{
          "name" => "POSTGRES_PASSWORD",
          "value" => "postgres"
        },
        %{
          "name" => "PGDATA",
          "value" => "/var/lib/postgresql/data/pgdata"
        }
      ],
      "volumeMounts" => [
        %{
          "name" => volume_name,
          "mountPath" => "/var/lib/postgresql/data"
        }
      ]
    }
  end

  # The mounted volume can't be written to by the pleroma user by default
  def generate_container(:init_volume, instance) do
    %{name: volume_name} = K8sVolume.get_volume_specs(:media, instance)

    %{
      "name" => "tribe-init-volume",
      "image" => "busybox",
      "command" => ["sh", "-c", "chown -R 100:100 /data"],
      "volumeMounts" => [
        %{
          "mountPath" => "/data",
          "name" => volume_name
        }
      ]
    }
  end
end
