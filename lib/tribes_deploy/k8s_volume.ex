defmodule TribesDeploy.K8sVolume do
  @moduledoc """
  Generates K8S volume manifests and attributes.
  """
  alias Tribes.Instance

  def generate_volumes(types, %Instance{} = instance) when is_list(types) do
    Enum.map(types, fn type -> generate_volume(type, instance) end)
  end

  def generate_volume(volume_type, %Instance{} = instance) do
    %{name: name} = get_volume_specs(volume_type, instance)

    %{
      "name" => name,
      "persistentVolumeClaim" => %{
        "claimName" => name
      }
    }
  end

  # TODO: Different limits depending on Tribe tier
  def get_volume_specs(:db, %Instance{}) do
    %{name: "tribe-volume-db", size: "30Gi"}
  end

  def get_volume_specs(:media, %Instance{}) do
    %{name: "tribe-volume-media", size: "20Gi"}
  end
end
