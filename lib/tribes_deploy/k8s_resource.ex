defmodule TribesDeploy.K8sResource do
  @moduledoc """
  Generates K8S resource manifests.
  """
  alias Tribes.Instance
  alias TribesDeploy.K8sContainer
  alias TribesDeploy.K8sVolume

  # The Namespace groups resources together. Each tribe gets a namespace.
  # https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/
  def generate_resource(:namespace, %Instance{} = instance) do
    %{
      "apiVersion" => "v1",
      "kind" => "Namespace",
      "metadata" => %{
        "name" => Instance.to_k8s_namespace(instance)
      }
    }
  end

  # The Service exposes a port from the Pod.
  # https://kubernetes.io/docs/concepts/services-networking/service/
  def generate_resource(:service, %Instance{domain: domain} = instance) do
    %{
      "apiVersion" => "v1",
      "kind" => "Service",
      "metadata" => %{
        "name" => "tribe-service",
        "namespace" => Instance.to_k8s_namespace(instance)
      },
      "spec" => %{
        "type" => "ClusterIP",
        "selector" => %{
          "domain" => domain
        },
        "ports" => [
          %{
            "port" => 4000
          }
        ]
      }
    }
  end

  # The Deployment creates the Pod.
  # https://kubernetes.io/docs/concepts/workloads/controllers/deployment/
  def generate_resource(:deployment, %Instance{domain: domain} = instance) do
    %{
      "apiVersion" => "apps/v1",
      "kind" => "Deployment",
      "metadata" => %{
        "name" => "tribe-deployment",
        "namespace" => Instance.to_k8s_namespace(instance)
      },
      "spec" => %{
        "replicas" => 1,
        "selector" => %{
          "matchLabels" => %{
            "domain" => domain
          }
        },
        "template" => %{
          "metadata" => %{
            "labels" => %{
              "domain" => domain
            }
          },
          "spec" => %{
            "initContainers" => [K8sContainer.generate_container(:init_volume, instance)],
            "containers" => K8sContainer.generate_containers([:pleroma, :postgres], instance),
            "volumes" => K8sVolume.generate_volumes([:db, :media], instance),
            "terminationGracePeriodSeconds" => 60
          }
        }
      }
    }
  end

  # The HTTPProxy routes traffic from the domain (Nginx).
  # https://projectcontour.io/docs/v1.10.0/config/fundamentals/
  def generate_resource(:httpproxy, %Instance{domain: domain} = instance) do
    %{
      "apiVersion" => "projectcontour.io/v1",
      "kind" => "HTTPProxy",
      "metadata" => %{
        "name" => "tribe-httpproxy",
        "namespace" => Instance.to_k8s_namespace(instance)
      },
      "spec" => %{
        "routes" => [
          %{
            "conditions" => [
              %{
                "prefix" => "/"
              }
            ],
            "enableWebsockets" => true,
            "services" => [
              %{
                "name" => "tribe-service",
                "port" => 4000
              }
            ]
          }
        ],
        "virtualhost" => %{
          "fqdn" => domain,
          "tls" => %{
            "secretName" => "tribe-tls-secret"
          }
        }
      }
    }
  end

  # The Certificate provisions SSL for the HTTPProxy.
  # https://cert-manager.io/docs/usage/certificate/
  def generate_resource(:certificate, %Instance{domain: domain} = instance) do
    %{
      "apiVersion" => "cert-manager.io/v1",
      "kind" => "Certificate",
      "metadata" => %{
        "name" => "tribe-certificate",
        "namespace" => Instance.to_k8s_namespace(instance)
      },
      "spec" => %{
        "secretName" => "tribe-tls-secret",
        "dnsNames" => [domain],
        "issuerRef" => %{
          "name" => "tribes-letsencrypt",
          "kind" => "ClusterIssuer"
        }
      }
    }
  end

  # A PersistentVolumeClaim stores data between reboots.
  # https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistentvolumeclaims
  def generate_resource({:volume, volume_type}, %Instance{} = instance) do
    %{name: name, size: size} = K8sVolume.get_volume_specs(volume_type, instance)

    %{
      "apiVersion" => "v1",
      "kind" => "PersistentVolumeClaim",
      "metadata" => %{
        "name" => name,
        "namespace" => Instance.to_k8s_namespace(instance)
      },
      "spec" => %{
        "accessModes" => ["ReadWriteOnce"],
        "resources" => %{
          "requests" => %{
            "storage" => size
          }
        },
        "storageClassName" => "longhorn"
      }
    }
  end
end
