defmodule TribesDeploy.DeployWorker do
  @moduledoc """
  Creates an instance in the K8S cluster.
  """
  use Tribes.BaseWorker, queue: :k8s, max_attempts: 10
  alias Tribes.{Instance, User}
  alias TribesDeploy.PingWorker

  @impl Oban.Worker
  def perform(%Oban.Job{args: %{"instance_id" => instance_id}}) do
    with %Instance{} = instance <- Instance.get_by_id(instance_id),
         {:ok, %Instance{} = instance} <- Instance.set_status(instance, :deploying),
         {:ok, _} = results <- TribesDeploy.deploy_instance(instance),
         {:ok, %Oban.Job{}} <- PingWorker.enqueue(instance),
         {:ok, _} <- deliver_email(instance) do
      results
    else
      error -> {:error, error}
    end
  end

  defp deliver_email(%Instance{user_id: user_id} = instance) do
    user_id
    |> User.get_by_id()
    |> Tribes.UserEmail.instance_deploying_email(instance)
    |> Tribes.Mailer.deliver()
  end
end
