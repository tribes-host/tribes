defmodule TribesDeploy.DestroyWorker do
  @moduledoc """
  Destroys an instance in the K8S cluster.
  """
  use Tribes.BaseWorker, queue: :k8s, max_attempts: 3
  alias Tribes.Instance

  @impl Oban.Worker
  def perform(%Oban.Job{args: %{"instance_id" => instance_id}}) do
    with %Instance{} = instance <- Instance.get_by_id(instance_id),
         {:ok, %Instance{} = instance} <- Instance.set_status(instance, :destroying),
         {:ok, _} <- Instance.cancel_subscription(instance),
         {:ok, _} <- destroy_k8s(instance),
         {:ok, %Instance{}} <- Instance.delete(instance) do
      :ok
    else
      error -> {:error, error}
    end
  end

  defp destroy_k8s(%Instance{} = instance) do
    case TribesDeploy.destroy_instance(instance) do
      {:error, :not_found} -> {:ok, :not_found}
      result -> result
    end
  end
end
