defmodule TribesDeploy.InitWorker do
  @moduledoc """
  Provisions an admin account on the instance and sends an email.
  """
  use Tribes.BaseWorker, queue: :k8s, max_attempts: 5
  alias Tribes.Instance
  alias Tribes.User
  alias TribesDeploy.K8sExec

  @impl Oban.Worker
  def perform(%Oban.Job{args: %{"instance_id" => instance_id}}) do
    with %Instance{user_id: user_id} = instance <- Instance.get_by_id(instance_id),
         %User{} = user <- User.get_by_id(user_id),
         {:ok, password} <- K8sExec.create_admin_user(instance) do
      user
      |> Tribes.UserEmail.instance_ready_email(instance, password)
      |> Tribes.Mailer.deliver()
    end
    |> parse_strict()
  end
end
