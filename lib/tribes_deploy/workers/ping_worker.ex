defmodule TribesDeploy.PingWorker do
  @moduledoc """
  Pings a newly deployed instance until it's online.
  """
  use Tribes.BaseWorker, queue: :scrape, max_attempts: 20
  alias Tribes.Instance
  alias TribesDeploy.InitWorker

  @impl Oban.Worker
  def perform(%Oban.Job{args: %{"instance_id" => instance_id}}) do
    case Instance.get_by_id(instance_id) do
      %Instance{status: :online} -> {:discard, "Already online"}
      %Instance{} = instance -> do_ping(instance)
      nil -> {:discard, "Not found"}
    end
  end

  defp do_ping(%Instance{domain: domain} = instance) do
    with {:ok, %{status_code: 200}} <- HTTPoison.get("https://#{domain}"),
         {:ok, %Instance{}} <- Instance.set_status(instance, :online),
         {:ok, %Oban.Job{}} <- InitWorker.enqueue(instance) do
      :ok
    else
      _ -> {:error, "Ping failed"}
    end
  end
end
