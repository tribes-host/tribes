defmodule Mix.Tribes do
  @apps [
    :restarter,
    :ecto,
    :ecto_sql,
    :postgrex,
    :db_connection,
    :hackney,
    :flake_id,
    :k8s
  ]

  @doc "Common functions to be reused in mix tasks"
  def start_tribes do
    Application.put_env(:phoenix, :serve_endpoints, false, persistent: true)

    Enum.each(@apps, &Application.ensure_all_started/1)

    children = [
      Tribes.Repo,
      TribesWeb.Telemetry,
      {Phoenix.PubSub, name: Tribes.PubSub},
      TribesWeb.Endpoint
    ]

    opts = [strategy: :one_for_one, name: Tribes.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
