defmodule Mix.Tasks.Tribes.Deploy do
  alias Tribes.Instance
  import Mix.Tribes
  use Mix.Task

  @shortdoc "Quickly deploy a domain"

  def run([domain]) do
    start_tribes()
    instance = %Instance{id: FlakeId.get(), domain: domain}

    with {:ok, _results} <- TribesDeploy.deploy_instance(instance) do
      IO.puts("Deployed https://#{domain}")
    else
      error ->
        IO.inspect(error)
        IO.puts("Deploy failed!")
        exit(1)
    end
  end
end
