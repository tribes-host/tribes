defmodule Mix.Tasks.Tribes.User do
  alias Tribes.User
  import Mix.Tribes
  use Mix.Task

  @shortdoc "Manage users"

  def run(["make_admin", email]) do
    start_tribes()

    with %User{} = user <- User.get_by_email(email),
         {:ok, %User{}} <- User.make_admin(user) do
      IO.puts("#{email} was made an admin.")
    else
      error ->
        IO.inspect(error)
        IO.puts("Failed to make #{email} an admin.")
        exit(1)
    end
  end
end
