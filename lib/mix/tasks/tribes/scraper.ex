defmodule Mix.Tasks.Tribes.Scraper do
  alias Tribes.Instance
  alias Tribes.Repo
  alias Tribes.Scraper
  import Mix.Tribes
  use Mix.Task

  @shortdoc "Manual scraper actions"

  def run(["scrape_all"]) do
    start_tribes()

    Instance
    |> Repo.all()
    |> Enum.each(fn instance -> Scraper.scrape_instance(instance) end)
  end
end
