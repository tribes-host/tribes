defmodule TribesWeb.Router do
  use TribesWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash

    # https://hexdocs.pm/phoenix_live_view/live-layouts.html
    plug :put_root_layout, {TribesWeb.LayoutView, :root}
    plug :put_layout, {TribesWeb.LayoutView, :page}

    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug TribesWeb.Plugs.CookieAuthPlug
  end

  pipeline :dashboard do
    plug :browser
    plug TribesWeb.Plugs.RequireLoginPlug
    plug :put_layout, {TribesWeb.LayoutView, :dashboard}
  end

  pipeline :admin do
    plug :browser
    plug TribesWeb.Plugs.RequireLoginPlug
    plug TribesWeb.Plugs.RequireAdminPlug
  end

  scope "/", TribesWeb do
    pipe_through :browser

    get "/", PageController, :index
    get "/start", PageController, :start
    get "/tos", PageController, :tos

    # get "/explore", PageController, :explore
    # get "/tribe/:domain", PageController, :tribe

    get "/register", AuthController, :registration_form
    post "/register", AuthController, :register
    get "/login", AuthController, :login_form
    post "/login", AuthController, :login
    get "/logout", AuthController, :logout

    get "/verify_email/:user_id/:token", AuthController, :verify_email
    get "/password_reset/:user_id/:token", AuthController, :password_reset_form
    post "/password_reset/:user_id/:token", AuthController, :password_reset
    get "/password_reset", AuthController, :send_password_reset_form
    post "/password_reset", AuthController, :send_password_reset

    get "/pay/:id/btc", PaymentRequestController, :btc
    get "/pay/:id", PaymentRequestController, :show
    post "/pay/:id", PaymentRequestController, :pay
  end

  # Authenticated routes
  scope "/dashboard", TribesWeb do
    pipe_through :dashboard

    get "/", DashboardController, :index

    get "/create", DashboardController, :create_form
    post "/create", DashboardController, :create

    get "/:domain", DashboardController, :index
    get "/:domain/manage", DashboardController, :manage
    get "/:domain/billing", DashboardController, :billing
    get "/:domain/debugging", DashboardController, :debugging

    get "/:domain/destroy", DashboardController, :destroy_form
    post "/:domain/destroy", DashboardController, :destroy
  end

  scope "/admin", TribesWeb do
    pipe_through :admin

    get "/", AdminController, :index
  end

  # Enables LiveDashboard only for development
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser
      live_dashboard "/phoenix", metrics: TribesWeb.Telemetry
      forward "/mailbox", Plug.Swoosh.MailboxPreview, base_path: "/dev/mailbox"
    end
  end
end
