defmodule TribesWeb.DirectoryLive do
  use TribesWeb, :live_view

  @impl true
  def mount(_params, _, socket) do
    {:ok, assign(socket, :filters, [])}
  end

  @impl true
  def handle_event("filter", %{"filters" => filters} = _value, socket) do
    {:noreply, assign(socket, :filters, filters)}
  end

  @impl true
  def handle_event("filter", _, socket) do
    {:noreply, assign(socket, :filters, [])}
  end
end
