defmodule TribesWeb.EmailFormLive do
  alias Tribes.EmailList
  use TribesWeb, :live_view

  @impl true
  def mount(_params, _, socket) do
    {:ok, assign(socket, :email, nil)}
  end

  @impl true
  def handle_event("email_subscribe", %{"email" => email}, socket) do
    with {:ok, %EmailList{email: ^email}} <- EmailList.subscribe(email) do
      {:noreply, assign(socket, email: email)}
    end
  end
end
