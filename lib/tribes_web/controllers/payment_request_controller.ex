defmodule TribesWeb.PaymentRequestController do
  use TribesWeb, :controller
  alias TribesPay.{Payment, PaymentRequest}

  plug TribesWeb.Plugs.RequireLoginPlug

  # FIXME: Users can only see their own PRs

  def show(conn, %{"id" => id}) do
    with %PaymentRequest{} = pr <- PaymentRequest.get_by_id(id) do
      render(conn, "show.html", pr: pr)
    end
  end

  def pay(conn, %{"id" => id, "stripeToken" => token}) do
    with %PaymentRequest{} = pr <- PaymentRequest.get_by_id(id) do
      case charge(pr, token) do
        {:ok, %Payment{} = payment, %PaymentRequest{} = pr} ->
          render(conn, "success.html", pr: pr, payment: payment)

        err ->
          render(conn, "show.html", pr: pr, err: err)
      end
    end
  end

  def btc(conn, %{"id" => id}) do
    with %PaymentRequest{} = pr <- PaymentRequest.get_by_id(id),
         {:ok, url} = Tribes.BTCPayGateway.bill(pr) do
      redirect(conn, external: url)
    end
  end

  defp charge(%PaymentRequest{} = pr, token) do
    opts = get_opts(pr, token)

    case Stripe.Charge.create(opts) do
      {:ok, %Stripe.Charge{status: "succeeded"} = charge} -> import_stripe_charge(charge)
    end
  end

  defp get_opts(
         %PaymentRequest{
           id: pr_id,
           user_id: user_id,
           amount: %Money{amount: amount, currency: currency}
         },
         token
       ) do
    %{
      amount: amount,
      currency: to_string(currency),
      source: token,
      metadata: %{
        "payment_request_id" => pr_id,
        "user_id" => user_id
      }
    }
  end

  defp import_stripe_charge(
         %Stripe.Charge{
           status: "succeeded",
           metadata: %{"payment_request_id" => pr_id, "user_id" => user_id},
           amount: amount,
           currency: currency
         } = charge
       ) do
    %Payment{
      user_id: user_id,
      payment_request_id: pr_id,
      amount: Money.new(amount, currency),
      amount_converted: Money.new(amount, currency),
      gateway: :stripe,
      gateway_data: charge
    }
    |> Payment.create()
  end
end
