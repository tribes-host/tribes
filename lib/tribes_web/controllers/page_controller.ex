defmodule TribesWeb.PageController do
  use TribesWeb, :controller
  alias Tribes.Instance
  alias Tribes.User

  def index(conn, _params) do
    conn
    |> put_layout({TribesWeb.LayoutView, :home})
    |> render("index.html")
  end

  def start(%{assigns: %{user: %User{}}} = conn, _params) do
    redirect(conn, to: "/dashboard/create")
  end

  def start(conn, _params) do
    render(conn, "start.html")
  end

  def explore(conn, _params) do
    render(conn, "explore.html")
  end

  def tribe(conn, %{"domain" => domain} = _params) do
    tribe = Instance.get_by_domain(domain)

    conn
    |> put_view(TribesWeb.InstanceView)
    |> render("show.html", instance: tribe)
  end

  def tos(conn, _params) do
    render(conn, "tos.html")
  end
end
