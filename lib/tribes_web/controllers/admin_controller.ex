defmodule TribesWeb.AdminController do
  use TribesWeb, :controller
  require Ecto.Query
  alias Tribes.{Repo, User, Instance}

  def index(conn, _params) do
    context = [
      user_count: Repo.aggregate(User, :count),
      instance_count: instance_count(),
      recent_users: recent_users(),
      recent_instances: recent_instances()
    ]

    render(conn, "index.html", context)
  end

  defp instance_count() do
    Instance
    |> Ecto.Query.where(status: :online)
    |> Repo.aggregate(:count)
  end

  defp recent_users() do
    User
    |> Ecto.Query.order_by(desc: :inserted_at)
    |> Ecto.Query.limit(20)
    |> Repo.all()
  end

  defp recent_instances() do
    Instance
    |> Ecto.Query.where(status: :online)
    |> Ecto.Query.order_by(desc: :inserted_at)
    |> Ecto.Query.limit(20)
    |> Repo.all()
  end
end
