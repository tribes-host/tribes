defmodule TribesWeb.AuthController do
  @moduledoc """
  Endpoints for logging in, logging out, and registering.
  """
  use TribesWeb, :controller
  alias Tribes.User
  alias TribesWeb.Session

  plug Hammer.Plug, [rate_limit: {"auth:register", 60_000, 10}, by: :ip] when action == :register

  plug Hammer.Plug, [rate_limit: {"auth:login", 60_000, 10}, by: :ip] when action == :login

  plug Hammer.Plug,
       [rate_limit: {"auth:verify_email", 60_000, 10}, by: :ip] when action == :verify_email

  plug(:check_registrations_open, nil when action in [:registration_form, :register])

  # Redirect an already logged-in user
  def registration_form(%{assigns: %{user: %User{}}} = conn, _) do
    redirect(conn, to: "/")
  end

  def registration_form(conn, _params) do
    render(conn, "registration.html")
  end

  def register(
        conn,
        %{"password" => pw, "password_confirmation" => pw, "accepts_tos" => "true"} = params
      ) do
    case User.register(params) do
      {:ok, %Session{token: token}} ->
        conn
        |> put_session(:session_token, token)
        |> redirect(to: "/")

      {:error, %Ecto.Changeset{errors: [{type, {message, _reason}} | _errors]}} ->
        conn
        |> put_flash(:error, "#{type} #{message}")
        |> redirect(to: "/register")

      _ ->
        conn
        |> put_flash(:error, "Registration failed.")
        |> redirect(to: "/register")
    end
  end

  def register(conn, %{"password" => pw, "password_confirmation" => pw}) do
    conn
    |> put_flash(:error, "Must accept Tribes Terms of Service.")
    |> redirect(to: "/register")
  end

  def register(conn, %{"email" => _email, "password" => _password}) do
    conn
    |> put_flash(:error, "Passwords don't match.")
    |> redirect(to: "/register")
  end

  def verify_email(conn, %{"user_id" => user_id, "token" => token}) do
    with %User{
           email_verification_token: ^token,
           email_verification_token_emailed_at: token_timestamp
         } = user <- User.get_by_id(user_id),
         {:ok, %User{} = user} <- User.verify_email(user),
         days when days <= 7 <- Timex.diff(get_naive_timestamp(), token_timestamp, :days),
         {:ok, %Session{token: token}} <- Session.create(user) do
      conn
      |> put_flash(:success, "Your account has been successfully confirmed!")
      |> put_session(:session_token, token)
      |> redirect(to: "/")
    else
      _ ->
        conn
        |> put_flash(:error, "This confirmation URL has expired.")
        |> redirect(to: "/")
    end
  end

  # Redirect an already logged-in user
  def login_form(%{assigns: %{user: %User{}}} = conn, _) do
    redirect(conn, to: "/")
  end

  def login_form(conn, _params) do
    render(conn, "login.html")
  end

  def login(conn, %{"email" => email, "password" => password} = _params) do
    with %User{} = user <- User.get_by_email(email),
         {:ok, %User{} = user} <- User.check_pass(user, password),
         {:ok, %Session{token: token}} <- Session.create(user) do
      conn
      |> put_session(:session_token, token)
      |> redirect(to: "/")
    else
      _ ->
        conn
        |> put_flash(:error, "Wrong username or password.")
        |> redirect(to: "/login")
    end
  end

  def logout(conn, _params) do
    conn
    |> clear_session()
    |> redirect(to: "/")
  end

  def send_password_reset_form(conn, _params) do
    render(conn, "password_reset_request.html")
  end

  def send_password_reset(conn, %{"email" => email} = _params) do
    with %User{} = user <- User.get_by_email(email),
         {:ok, _} <- User.send_password_reset(user) do
      conn
      |> put_flash(:info, "Password Reset: Check your email for further instructions.")
      |> redirect(to: "/")
    else
      nil ->
        conn
        |> put_flash(:error, "Email address not in database.")
        |> redirect(to: "/password_reset")
    end
  end

  def password_reset_form(conn, _params) do
    render(conn, "password_reset.html")
  end

  def password_reset(conn, %{
        "user_id" => user_id,
        "token" => token,
        "password" => password,
        "password_confirmation" => password_confirmation
      })
      when password == password_confirmation do
    with %User{password_reset_token: ^token, password_reset_token_emailed_at: token_timestamp} =
           user <- User.get_by_id(user_id),
         {:ok, %User{} = user} <- User.change_password(user, password),
         days when days <= 7 <- Timex.diff(get_naive_timestamp(), token_timestamp, :days),
         {:ok, %Session{token: token}} <- Session.create(user) do
      conn
      |> put_flash(:success, "Your password has been successfully updated.")
      |> put_session(:session_token, token)
      |> redirect(to: "/")
    else
      _ ->
        conn
        |> put_flash(:error, "This URL has expired.")
        |> redirect(to: "/")
    end
  end

  def password_reset(conn, %{
        "user_id" => user_id,
        "token" => token,
        "password" => _password,
        "password_confirmation" => _password_confirmation
      }) do
    conn
    |> put_flash(:error, "Passwords don't match.")
    |> redirect(to: "/password_reset/#{user_id}/#{token}")
  end

  def password_reset(conn, _params) do
    conn
    |> put_flash(:error, "Password reset failed.")
    |> redirect(to: "/")
  end

  defp get_naive_timestamp(), do: NaiveDateTime.truncate(NaiveDateTime.utc_now(), :second)

  defp check_registrations_open(conn, _) do
    if Tribes.Config.get(:registrations_open) do
      conn
    else
      conn
      |> put_flash(:error, "Registations are currently closed.")
      |> redirect(to: "/login")
      |> halt()
    end
  end
end
