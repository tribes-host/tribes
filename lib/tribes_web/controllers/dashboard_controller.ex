defmodule TribesWeb.DashboardController do
  @moduledoc """
  The dashboard lets users manage their account and deploy instances.
  """
  use TribesWeb, :controller
  alias Tribes.Instance
  alias Tribes.User
  alias TribesDeploy.DestroyWorker
  alias TribesPay.Billing
  alias TribesPay.PaymentRequest
  alias TribesPay.Subscription

  def index(%{assigns: %{user: %User{}}} = conn, %{"domain" => _} = params) do
    render_authenticated(conn, params, "index.html")
  end

  def index(%{assigns: %{user: %User{} = user}} = conn, _params) do
    with [%Instance{domain: domain} | _] <- Instance.get_for_user(user, [:deploying, :online]) do
      redirect(conn, to: "/dashboard/#{domain}")
    else
      _ -> render(conn, "index.html", instance: nil)
    end
  end

  def create_form(conn, _params) do
    conn
    |> put_layout({TribesWeb.LayoutView, :page})
    |> render("create.html")
  end

  def create(%{assigns: %{user: %User{} = user}} = conn, %{"domain" => domain}) do
    # TODO: Make this dynamic
    tier = :small

    with {:ok, %PaymentRequest{id: id}} <- create_and_bill(user, domain, tier) do
      redirect(conn, to: "/pay/#{id}")
    end
  end

  defp create_and_bill(user, domain, tier) do
    today = Date.utc_today()

    Tribes.Repo.transaction(fn ->
      {:ok, %Subscription{} = subscription} = Subscription.create(user, tier)
      {:ok, %Instance{}} = Instance.create(subscription, domain)
      {:ok, %PaymentRequest{} = payment_request} = Billing.bill_for_date(subscription, today)
      payment_request
    end)
  end

  def destroy_form(conn, params) do
    render_authenticated(conn, params, "destroy.html")
  end

  def destroy(%{assigns: %{user: %User{} = user}} = conn, %{
        "domain" => domain,
        "confirm" => "yes"
      }) do
    with %Instance{} = instance <- Instance.get_by_domain(user, domain),
         {:ok, %Oban.Job{}} <- DestroyWorker.enqueue(instance) do
      conn
      |> put_flash(:info, "#{domain} is being destroyed.")
      |> redirect(to: "/dashboard")
    end
  end

  def manage(conn, params) do
    render_authenticated(conn, params, "manage.html")
  end

  def billing(conn, params) do
    render_authenticated(conn, params, "billing.html")
  end

  def debugging(conn, params) do
    render_authenticated(conn, params, "debugging.html")
  end

  defp render_authenticated(
         %{assigns: %{user: %User{} = user}} = conn,
         %{"domain" => domain},
         template
       ) do
    with %Instance{} = instance <- Instance.get_by_domain(user, domain) do
      render(conn, template, instance: instance)
    else
      _ -> redirect(conn, to: "/dashboard")
    end
  end
end
