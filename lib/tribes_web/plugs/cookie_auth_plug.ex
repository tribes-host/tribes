defmodule TribesWeb.Plugs.CookieAuthPlug do
  @moduledoc """
  Assigns a user to the conn according to their cookies.
  """
  import Plug.Conn
  alias Tribes.User
  alias TribesWeb.Session

  @behaviour Plug

  def init(_), do: nil

  def call(conn, _) do
    with token when not is_nil(token) <- get_session(conn, :session_token),
         %Session{user: %User{} = user} <- Session.get_by_token(token) do
      assign(conn, :user, user)
    else
      _ -> conn
    end
  end
end
