defmodule TribesWeb.Plugs.RequireLoginPlug do
  @moduledoc """
  Redirects to `/login` unless the user has been authenticated.
  """
  alias Tribes.User

  @behaviour Plug

  def init(_), do: nil

  def call(conn, _) do
    with %{assigns: %{user: %User{}}} <- conn do
      conn
    else
      _ ->
        conn
        |> Phoenix.Controller.redirect(to: "/login")
        |> Plug.Conn.halt()
    end
  end
end
