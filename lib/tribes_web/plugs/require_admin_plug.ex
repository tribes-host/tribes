defmodule TribesWeb.Plugs.RequireAdminPlug do
  @moduledoc """
  Only displays the page to admin users.
  """
  alias Tribes.User

  @behaviour Plug

  def init(_), do: nil

  def call(conn, _) do
    with %{assigns: %{user: %User{admin: true}}} <- conn do
      conn
    else
      _ ->
        conn
        |> Phoenix.Controller.redirect(to: "/login")
        |> Plug.Conn.halt()
    end
  end
end
