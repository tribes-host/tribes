defmodule TribesWeb.Session do
  @moduledoc """
  Session data for a user.
  """
  use Ecto.Schema
  alias Tribes.Repo
  alias Tribes.User
  alias TribesWeb.Session
  import Ecto.Query

  schema "sessions" do
    field(:token, :string)
    belongs_to(:user, User, type: FlakeId.Ecto.CompatType)

    timestamps()
  end

  def get_by_token(token) do
    Session
    |> where(token: ^token)
    |> preload(:user)
    |> Repo.one()
  end

  def create(%User{id: user_id}) do
    %Session{
      token: generate_token(),
      user_id: user_id
    }
    |> Repo.insert()
  end

  def generate_token() do
    :crypto.strong_rand_bytes(32)
    |> Base.url_encode64(padding: false)
  end
end
