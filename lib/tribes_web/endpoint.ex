defmodule TribesWeb.Endpoint do
  use Phoenix.Endpoint, otp_app: :tribes

  # The session will be stored in the cookie and signed,
  # this means its contents can be read but not tampered with.
  # Set :encryption_salt if you would also like to encrypt it.
  @session_options [
    store: :cookie,
    key: "_tribes_key",
    signing_salt: "/qOGBgBf",
    max_age: 60 * 60 * 24 * 365 * 100
  ]

  plug(TribesWeb.RemoteIpPlug)

  socket "/socket", TribesWeb.UserSocket,
    websocket: true,
    longpoll: false

  socket "/live", Phoenix.LiveView.Socket, websocket: [connect_info: [session: @session_options]]

  # Serve at "/" the static files from "priv/static" directory.
  #
  # You should set gzip to true if you are running phx.digest
  # when deploying your static files in production.
  plug Plug.Static,
    at: "/",
    from: :tribes,
    gzip: false,
    only: ~w(css fonts images js favicon.ico favicon.png robots.txt)

  # Media uploads
  plug Plug.Static, at: "/media", from: "media/"

  # Code reloading can be explicitly enabled under the
  # :code_reloader configuration of your endpoint.
  if code_reloading? do
    socket "/phoenix/live_reload/socket", Phoenix.LiveReloader.Socket
    plug Phoenix.LiveReloader
    plug Phoenix.CodeReloader
    plug Phoenix.Ecto.CheckRepoStatus, otp_app: :tribes
  end

  plug Phoenix.LiveDashboard.RequestLogger,
    param_key: "request_logger",
    cookie_key: "request_logger"

  plug Plug.RequestId
  plug Plug.Telemetry, event_prefix: [:phoenix, :endpoint]

  # BTCPay webhooks
  plug BTCPay.WebhookPlug,
    at: "/webhook/btcpay",
    handler: TribesPay.BTCPayHandler,
    secret: {Tribes.Config, :get, [:btcpay_webhook_secret]}

  plug Plug.Parsers,
    parsers: [:urlencoded, :multipart, :json],
    pass: ["*/*"],
    json_decoder: Phoenix.json_library()

  plug Plug.MethodOverride
  plug Plug.Head
  plug Plug.Session, @session_options
  plug TribesWeb.Router
end
