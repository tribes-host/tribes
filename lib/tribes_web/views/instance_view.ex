defmodule TribesWeb.InstanceView do
  alias Tribes.Instance
  use TribesWeb, :view

  def render("show.html", %{instance: %Instance{} = instance}) do
    assigns = %{
      tribe: instance,
      data: instance.remote_data["instance"],
      admin: instance.remote_data["admin"],
      featured_users: instance.remote_data["featured_users"] || [],
      feed_url: get_feed_url(instance),
      feed_items: instance.remote_data["feed"] || []
    }

    render(TribesWeb.PageView, "tribe.html", assigns)
  end

  defp get_feed_url(%Instance{} = instance) do
    case instance.feed_type do
      "user" -> "https://#{instance.domain}/@#{instance.feed_user_nickname}"
      "local" -> "https://#{instance.domain}/timeline/local"
    end
  end
end
