defmodule TribesWeb.AuthHelpers do
  @moduledoc """
  Conveniences for checking the login state of the user.
  """

  @doc """
  Checks if the user is logged in.
  """
  def logged_in(conn) do
    with %{assigns: %{user: %Tribes.User{}}} <- conn do
      true
    else
      _ -> false
    end
  end

  @doc """
  Gets the logged in user.
  """
  def get_user(conn) do
    with %{assigns: %{user: %Tribes.User{} = user}} <- conn do
      user
    else
      _ -> nil
    end
  end

  @doc """
  Checks if the user is an admin.
  """
  def is_admin(conn) do
    with %{assigns: %{user: %Tribes.User{admin: true}}} <- conn do
      true
    else
      _ -> false
    end
  end
end
