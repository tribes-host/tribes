defmodule TribesDeploy do
  @moduledoc """
  CRUD operations for K8S resources.
  """
  alias Tribes.Instance
  alias TribesDeploy.K8sResource

  def deploy_instance(%Instance{} = instance) do
    with {:ok, _} <- validate_instance(instance),
         {:ok, conn} <- K8s.Conn.lookup(:tribes) do
      resource_types = [
        :namespace,
        {:volume, :db},
        {:volume, :media},
        :deployment,
        :service,
        :httpproxy,
        :certificate
      ]

      Enum.map(resource_types, fn resource_type ->
        resource_type
        |> K8sResource.generate_resource(instance)
        |> get_or_create(conn)
      end)
      |> parse_results()
    end
  end

  def destroy_instance(%Instance{} = instance) do
    with {:ok, conn} <- K8s.Conn.lookup(:tribes) do
      :namespace
      |> K8sResource.generate_resource(instance)
      |> K8s.Client.delete()
      |> K8s.Client.run(conn)
    end
  end

  # Util function to return {:ok, _} when a resource already exists
  defp get_or_create(resource, conn) do
    opts = [recv_timeout: Tribes.Config.get(:k8s_run_timeout, 600_000)]

    resource
    |> K8s.Client.get()
    |> K8s.Client.run(conn, opts)
    |> case do
      {:ok, _} = result ->
        result

      _ ->
        resource
        |> K8s.Client.create()
        |> K8s.Client.run(conn, opts)
    end
  end

  defp validate_instance(%Instance{domain: domain} = instance) do
    cname = Tribes.Config.get(:cname) |> String.trim(".")

    if cname != domain do
      {:ok, instance}
    else
      {:error, "Invalid domain name!"}
    end
  end

  defp parse_results(results) when is_list(results) do
    if allOK?(results) do
      {:ok, results}
    else
      {:error, results}
    end
  end

  # allOK?([ok: _, ok: _, ok: _, ok: _, ok: _]) -> true
  defp allOK?(results) when is_list(results) do
    Enum.all?(results, fn result ->
      match?({:ok, _}, result)
    end)
  end
end
