for struct <- Stripe.Converter.structs() do
  defimpl Jason.Encoder, for: struct do
    def encode(value, opts) do
      Jason.Encode.map(Map.delete(value, :__struct__), opts)
    end
  end
end
