defmodule TribesPay.BTCPayGateway do
  alias TribesPay.PaymentRequest

  def bill(%PaymentRequest{} = payment_request) do
    with {:ok, %BTCPay.Invoice{} = invoice} <- create_invoice(payment_request) do
      {:ok, BTCPay.Invoice.pay_url(invoice)}
    end
  end

  def create_invoice(%PaymentRequest{id: id, amount: %Money{amount: amount, currency: :USD}}) do
    store = Tribes.Config.get(:btcpay_store)

    %BTCPay.Invoice{
      # TODO: Make this work for other currencies than USD.
      amount: amount / 100,
      currency: "usd",
      metadata: %{
        tribes_payment_request: id
      },
      checkout: %{
        redirectURL: "https://tribes.host/dashboard"
      }
    }
    |> BTCPay.Invoice.create(store)
  end
end
