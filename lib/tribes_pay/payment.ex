defmodule TribesPay.Payment do
  @moduledoc """
  A payment through some gateway, mainly logged for review by the user and
  for bookkeeping purposes.
  """
  use Ecto.Schema
  alias __MODULE__
  alias Tribes.Repo
  alias Tribes.User
  alias TribesPay.PaymentHandler
  alias TribesPay.PaymentRequest

  @primary_key {:id, FlakeId.Ecto.Type, autogenerate: true}

  schema "payments" do
    belongs_to(:user, User, type: FlakeId.Ecto.CompatType)
    belongs_to(:payment_request, PaymentRequest, type: FlakeId.Ecto.CompatType)

    field :amount, Money.Ecto.Composite.Type, null: false
    field :amount_converted, Money.Ecto.Composite.Type, null: false

    field :gateway, Ecto.Enum, values: [:btcpay, :stripe], null: false
    field :gateway_data, :map

    timestamps()
  end

  def create(%Payment{payment_request: %PaymentRequest{} = pr} = payment) do
    with {:ok, payment} <- Repo.insert(payment),
         {:ok, updated_pr} <- PaymentRequest.refresh_status(pr),
         {:ok, _} <- PaymentHandler.post_payment_action(pr, updated_pr) do
      {:ok, payment, updated_pr}
    else
      error -> {:error, error}
    end
  end

  def create(%Payment{} = payment) do
    payment
    |> Repo.preload(:payment_request)
    |> create()
  end

  def sum_converted(payments, currency) when is_list(payments) do
    Enum.reduce(payments, Money.new(0, currency), fn payment, acc ->
      case payment do
        %Payment{amount_converted: amount} -> Money.add(acc, amount)
        _ -> raise "input wasn't a Payment struct"
      end
    end)
  end
end
