defmodule TribesPay.Subscription do
  @moduledoc """
  The payment abstraction over a tribe. PaymentRequests are generated from this.
  """
  use Ecto.Schema
  alias Tribes.Repo
  alias Tribes.User
  alias TribesPay.Subscription

  @primary_key {:id, FlakeId.Ecto.Type, autogenerate: true}

  schema "subscriptions" do
    belongs_to(:user, User, type: FlakeId.Ecto.CompatType)

    field :status, Ecto.Enum, values: [:active, :cancelled], null: false
    field :tier, Ecto.Enum, values: [:small]
    field :billing_day, :integer, null: false

    timestamps()
  end

  def to_price(%Subscription{tier: tier}) do
    with {amount, currency} <- Tribes.Config.get([:prices, tier]) do
      Money.new(amount, currency)
    end
  end

  def create(%User{id: user_id}, tier) do
    %{day: billing_day} = Date.utc_today()

    %Subscription{
      status: :active,
      user_id: user_id,
      tier: tier,
      billing_day: billing_day
    }
    |> Repo.insert()
  end
end
