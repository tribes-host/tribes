defmodule TribesPay.Billing do
  @moduledoc """
  A glue layer between Subscriptions and PaymentRequests.
  """
  require Ecto.Query
  alias Tribes.Repo
  alias TribesPay.PaymentRequest
  alias TribesPay.Subscription

  @doc "Bills any unbilled Subscriptions this month up to this day."
  @spec bill_all_for_date(Date.t()) :: nil
  def bill_all_for_date(%Date{} = date) do
    date
    |> unbilled_subscriptions_query()
    # TODO: Chunk stream
    |> Repo.all()
    |> Enum.map(&bill_for_date(&1, date))
  end

  @doc "Query for Subscriptions without any PaymentRequests this month up to this day."
  @spec unbilled_subscriptions_query(Date.t()) :: Ecto.Query.t()
  def unbilled_subscriptions_query(date) do
    billing_period = to_billing_period(date)
    billing_day = max_billing_day(date)

    Subscription
    |> Ecto.Query.where([s], s.status == :active)
    |> Ecto.Query.where([s], s.billing_day <= ^billing_day)
    |> Ecto.Query.preload(:user)
    |> Ecto.Query.join(:left, [s], pr in PaymentRequest, on: s.id == pr.subscription_id)
    |> Ecto.Query.where(
      [s, pr],
      fragment("?.billing_period != ? OR ?.billing_period IS NULL", pr, ^billing_period, pr)
    )
  end

  @spec bill_for_date(Subscription.t(), Date.t()) :: PaymentRequest.t()
  def bill_for_date(%Subscription{id: subscription_id, user_id: user_id} = subscription, date) do
    %PaymentRequest{
      user_id: user_id,
      subscription_id: subscription_id,
      status: :unpaid,
      amount: Subscription.to_price(subscription),
      billing_period: to_billing_period(date)
    }
    |> Repo.insert()
  end

  @doc "Returns 31 if the date is the last day of the month."
  @spec max_billing_day(Date.t()) :: integer()
  def max_billing_day(%Date{day: day, month: 2}) when day == 29, do: 31

  def max_billing_day(%Date{day: day, month: 2, year: year}) when day == 28 do
    if rem(year, 4) == 0, do: 28, else: 31
  end

  def max_billing_day(%Date{day: day, month: month}) when month in [4, 6, 9, 11] and day == 30,
    do: 31

  def max_billing_day(%Date{day: day}) when day > 31, do: 31
  def max_billing_day(%Date{day: day}), do: day

  @doc "Converts a date into a billing period string."
  def to_billing_period(%Date{year: year, month: month}) do
    "#{year}-#{month}"
  end
end
