defmodule TribesPay.BillingWorker do
  @moduledoc """
  Creates a PaymentRequest for due instances.
  """
  use Tribes.BaseWorker, queue: :billing, max_attempts: 5

  @impl Oban.Worker
  def perform(%Oban.Job{}) do
    Date.utc_today()
    |> TribesPay.Billing.bill_all_for_date()
  end
end
