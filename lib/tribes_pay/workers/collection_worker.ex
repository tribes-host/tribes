defmodule TribesPay.CollectionWorker do
  @moduledoc """
  Attempts to charge cards, send reminder emails,
  and de-provision uncollectable instances.
  """
  use Tribes.BaseWorker, queue: :billing, max_attempts: 5

  @impl Oban.Worker
  def perform(%Oban.Job{}) do
    # TODO
    :ok
  end
end
