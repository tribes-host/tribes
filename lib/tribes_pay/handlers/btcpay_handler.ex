defmodule TribesPay.BTCPayHandler do
  @moduledoc """
  BTCPay webhook responses.
  """
  alias TribesPay.Payment
  alias TribesPay.PaymentRequest

  def handle_event(%{"type" => "InvoiceReceivedPayment"} = event) do
    with {:ok, %BTCPay.Invoice{} = invoice} <- refetch_invoice(event),
         %Payment{} = payment <- generate_payment(invoice, event),
         {:ok, %Payment{}, %PaymentRequest{}} <- Payment.create(payment) do
      :ok
    end
  end

  def handle_event(_event), do: :ok

  defp generate_payment(%BTCPay.Invoice{amount: amount, currency: currency} = invoice, event) do
    with %PaymentRequest{id: pr_id, user_id: user_id} = pr <- get_payment_request(invoice),
         {:ok, parsed_amount} <- Money.parse(amount, currency) do
      %Payment{
        user_id: user_id,
        payment_request_id: pr_id,
        payment_request: pr,
        amount: parsed_amount,
        amount_converted: parsed_amount,
        gateway: :btcpay,
        gateway_data: event
      }
    end
  end

  defp refetch_invoice(%{"invoiceId" => invoice_id, "storeId" => store_id}) do
    BTCPay.Invoice.get(invoice_id, store_id)
  end

  defp get_payment_request(%BTCPay.Invoice{metadata: %{tribes_payment_request: id}}) do
    PaymentRequest.get_by_id(id)
  end
end
