defmodule TribesPay.PaymentHandler do
  @moduledoc """
  Handles post-payment actions.
  """
  alias Tribes.Instance
  alias TribesDeploy.DeployWorker
  alias TribesPay.PaymentRequest

  def post_payment_action(%PaymentRequest{status: :unpaid}, %PaymentRequest{
        status: :paid,
        subscription_id: subscription_id
      }) do
    case Instance.get_by_subscription_id(subscription_id) do
      %Instance{status: :draft} = instance -> DeployWorker.enqueue(instance)
      %Instance{} = instance -> {:ok, instance}
      _ -> {:error, "No instance found for subscription."}
    end
  end

  def post_payment_action(_, _), do: {:ok, nil}
end
