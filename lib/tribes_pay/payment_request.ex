defmodule TribesPay.PaymentRequest do
  @moduledoc """
  A request for funds, perhaps with a consequence if unpaid (eg de-provisioning
  of an instance). All Payments must be towards a PaymentRequest for bookkeeping
  purposes.
  """
  use Ecto.Schema
  import Ecto.Changeset
  import Ecto.Query
  alias Tribes.Repo
  alias Tribes.User
  alias TribesPay.Payment
  alias TribesPay.PaymentRequest
  alias TribesPay.Subscription

  @primary_key {:id, FlakeId.Ecto.Type, autogenerate: true}

  schema "payment_requests" do
    belongs_to(:user, User, type: FlakeId.Ecto.CompatType)
    belongs_to(:subscription, Subscription, type: FlakeId.Ecto.CompatType)
    field :status, Ecto.Enum, values: [:unpaid, :paid], null: false, default: :unpaid
    field :amount, Money.Ecto.Composite.Type, null: false
    field :billing_period, :string, null: false

    timestamps()
  end

  def get_by_id(id) do
    Repo.get(PaymentRequest, id)
  end

  def get_payments(%PaymentRequest{id: id}) do
    Payment
    |> where(payment_request_id: ^id)
    |> Repo.all()
  end

  def set_status(%PaymentRequest{} = payment_request, status) do
    payment_request
    |> change(status: status)
    |> Repo.update()
  end

  def refresh_status(
        %PaymentRequest{amount: %Money{currency: currency} = amount} = payment_request
      ) do
    sum =
      payment_request
      |> get_payments()
      |> Payment.sum_converted(currency)

    status = if Money.compare(sum, amount) >= 0, do: :paid, else: :unpaid

    case payment_request do
      %{status: ^status} -> {:ok, payment_request}
      payment_request -> set_status(payment_request, status)
    end
  end
end
