# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :tribes,
  ecto_repos: [Tribes.Repo],
  registrations_open: false,
  support_email: "support@tribes.host",
  notification_email: "tribes@tribes.host",
  prices: [small: {10_00, :USD}],
  cname: "tribe.tribes.host.",
  k8s_run_timeout: 600_000

config :tribes, Tribes.Scraper, http_client: HTTPoison

# Configures the endpoint
config :tribes, TribesWeb.Endpoint,
  url: [host: "localhost"],
  render_errors: [view: TribesWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Tribes.PubSub,
  live_view: [signing_salt: "50krGPdZ"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Kubernetes
config :k8s,
  clusters: %{
    tribes: %{
      conn: "~/.kube/config"
    }
  }

# Oban
config :tribes, Oban,
  repo: Tribes.Repo,
  queues: [k8s: 10, billing: 10, scrape: 5],
  crontab: [
    {"@hourly", TribesPay.BillingWorker},
    {"@hourly", TribesPay.CollectionWorker}
  ]

# Money
config :money,
  custom_currencies: [
    BTC: %{name: "Bitcoin", symbol: "₿", exponent: 8}
  ]

config :tribes, TribesWeb.RemoteIpPlug,
  enabled: true,
  headers: ["x-forwarded-for"],
  proxies: [],
  reserved: [
    "127.0.0.0/8",
    "::1/128",
    "fc00::/7",
    "10.0.0.0/8",
    "172.16.0.0/12",
    "192.168.0.0/16"
  ]

config :hammer,
  backend: {Hammer.Backend.ETS, [expiry_ms: 60_000 * 60 * 4, cleanup_interval_ms: 60_000 * 10]}

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
