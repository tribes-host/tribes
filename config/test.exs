import Config

config :tribes, Tribes.Scraper, http_client: Tribes.HTTPoisonMock

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :tribes, Tribes.Repo,
  url: "postgres://postgres:postgres@postgres/tribes_test",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :tribes, TribesWeb.Endpoint,
  secret_key_base: "Tn9T3Ye3ywfVvSfgbmRznrMHU5C7FfN1YkLp7uVHJiJBleYw2qw5IqISAp8FQUUE",
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Send emails to test mailbox
config :tribes, Tribes.Mailer, adapter: Swoosh.Adapters.Test

# Oban: disable plugins, scheduled jobs, and job dispatching altogether
config :tribes, Oban, crontab: false, queues: false, plugins: false

# Mock K8s responses
config :k8s,
  discovery_driver: K8s.Discovery.Driver.File,
  discovery_opts: [config: "test/fixtures/k8s-discovery.json"],
  http_provider: K8s.Client.DynamicHTTPProvider,
  clusters: %{
    tribes: %{
      conn: "test/fixtures/kube-config.yaml"
    }
  }
