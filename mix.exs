defmodule Tribes.MixProject do
  use Mix.Project

  def project do
    [
      app: :tribes,
      version: "0.1.0",
      elixir: "~> 1.7",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Tribes.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:phoenix, "~> 1.5.3"},
      {:phoenix_ecto, "~> 4.2"},
      {:ecto_sql, "~> 3.5"},
      {:postgrex, ">= 0.0.0"},
      {:phoenix_html, "~> 2.11"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:phoenix_live_dashboard, "~> 0.2.0"},
      {:phoenix_live_view, "~> 0.14.1"},
      {:floki, ">= 0.0.0", only: :test},
      {:telemetry_metrics, "~> 0.4"},
      {:telemetry_poller, "~> 0.4"},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
      {:plug_cowboy, "~> 2.0"},
      {:httpoison, "~> 1.6"},
      {:timex, "~> 3.6.2"},
      {:fast_sanitize, "~> 0.2.2"},
      {:pbkdf2_elixir, "~> 1.2"},
      {:ex_machina, "~> 2.4", only: :test},
      {:flake_id, "~> 0.1.0"},
      {:swoosh, "~> 1.1"},
      {:gen_smtp, "~> 1.1"},
      {:k8s,
       git: "https://github.com/bradleyd/k8s", ref: "c37d3acb99b0483473b495a9b9f25111cae57042"},
      {:convertat, "~> 1.1"},
      {:oban, "~> 2.3"},
      {:uuid, "~> 1.1"},
      {:btcpay,
       git: "https://gitlab.com/tribes-host/btcpay_elixir",
       ref: "0220e3bbb72949e4e8fb456256ec07feb6ea0ac0"},
      {:money, "~> 1.8"},
      {:stripity_stripe, "~> 2.9"},
      {:remote_ip, "~> 0.2.1"},
      {:hammer, "~> 6.0"},
      {:hammer_plug, "~> 2.1"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to install project dependencies and perform other setup tasks, run:
  #
  #     $ mix setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      setup: ["deps.get", "ecto.setup", "cmd npm install --prefix assets"],
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate --quiet", "test"]
    ]
  end
end
