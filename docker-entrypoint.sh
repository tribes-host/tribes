#!/bin/sh
set -e

echo "-- Running migrations..."
until mix ecto.migrate
do
  echo "-- Waiting for database..."
  sleep 1s
done

echo "-- Starting!"
mix phx.server
